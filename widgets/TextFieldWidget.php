<?php

namespace GO1\FormCenter\FieldWidget;

use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Field\FieldOptions;
use GO1\FormCenter\Field\Widget\FieldWidgetBase;

class TextFieldWidget extends FieldWidgetBase
{

    /** @var string */
    protected $name = 'textfield';

    /** @var string */
    protected $humanName = 'Text field';

    public function render(FieldInterface $field, FieldOptions $fieldOptions, array $fieldValueItems = [], array $params = [])
    {
        $params['tag'] = 'input';
        $params['closeTag'] = false;
        return parent::render($field, $fieldOptions, $params);
    }

    public function getSupportedFieldTypes()
    {
        $fieldTypes = parent::getSupportedFieldTypes();
        if (!in_array('textfield', $fieldTypes)) {
            $fieldTypes[] = 'textfield';
        }
        return $fieldTypes;
    }

}
