<?php

namespace GO1\FormCenter\Entity\Provider;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\Type\FieldTypeInterface;
use GoCatalyze\SyncCenter\ServiceInterface;
use RuntimeException;

/**
 * Event aware entity-type provider.
 *
 */
abstract class EntityTypeProviderBase implements EntityTypeProviderInterface
{

    use \AndyTruong\Event\EventAwareTrait,
        \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var EntityTypeInterface */
    private $entityTypes;

    /** @var ServiceInterface */
    private $remoteService;

    /**
     * {@inheritdoc}
     * @return EntityTypeInterface[]
     */
    public function getEntityTypes()
    {
        if (null === $this->entityTypes) {
            $this->entityTypes = $this->discoverEntityTypes();
        }
        return $this->entityTypes;
    }

    /**
     * {@inheritdoc}
     * @param EntityTypeInterface $entity_type
     * @throws \RuntimeException
     * @return EntityTypeProviderInterface
     */
    public function registerEntityType(EntityTypeInterface $entity_type)
    {
        $name = $entity_type->getName();
        if (isset($this->entityTypes[$name])) {
            throw new RuntimeException('Entity type is already registered: ' . $name);
        }
        $this->entityTypes[$name] = $entity_type;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $name
     * @return EntityTypeProviderBase
     */
    public function unregisterEntityType($name)
    {
        if ($this->isEntityTypeRegistered($name)) {
            unset($this->entityTypes[$name]);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $name
     * @return bool
     */
    public function isEntityTypeRegistered($name)
    {
        return isset($this->entityTypes[$name]);
    }

    /**
     * Convert remote entity type to local.
     *
     * @param string $remoteEntityType
     * @return FieldTypeInterface
     */
    public function stringToFieldType($remoteEntityType)
    {
        $eventName = "entityTypeProvider.stringToFieldType.{$this->name}";
        $fieldType = $this->collectResults($eventName, $this, ['remoteEntityType' => $remoteEntityType])[0];
        if ($fieldType instanceof FieldTypeInterface) {
            return $fieldType;
        }
        throw new RuntimeException('Unsupported field type: ' . $remoteEntityType);
    }

    /**
     * Inject remote service.
     *
     * @param ServiceInterface $service
     */
    public function setRemoteService(ServiceInterface $service)
    {
        $this->remoteService = $service;
    }

    /**
     * Get remote service object.
     *
     * @return ServiceInterface
     */
    public function getRemoteService()
    {
        return $this->remoteService;
    }

}
