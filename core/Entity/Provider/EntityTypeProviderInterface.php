<?php

namespace GO1\FormCenter\Entity\Provider;

use AndyTruong\Event\DispatcherAwareInterface;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\Type\FieldTypeInterface;

interface EntityTypeProviderInterface extends DispatcherAwareInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Get available entity types.
     *
     * @return EntityTypeInterface[]
     */
    public function getEntityTypes();

    /**
     * Fetch remote system for entity types.
     *
     * @return EntityTypeInterface[]
     */
    public function discoverEntityTypes();

    /**
     * Register an entity-type with provider.
     *
     * @param EntityTypeInterface $entityType
     * @return EntityTypeProviderInterface
     */
    public function registerEntityType(EntityTypeInterface $entityType);

    /**
     * Unregister an entity type.
     *
     * @param string $name
     * @return EntityTypeProviderInterface
     */
    public function unregisterEntityType($name);

    /**
     * Check status of a entity-type in provider.
     *
     * @param string $name
     * @return bool
     */
    public function isEntityTypeRegistered($name);

    /**
     * Convert remote entity type to local.
     *
     * @param string $remoteEntityType
     * @return FieldTypeInterface
     */
    public function stringToFieldType($remoteEntityType);
}
