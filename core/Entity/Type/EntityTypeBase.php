<?php

namespace GO1\FormCenter\Entity\Type;

class EntityTypeBase implements EntityTypeInterface
{

    use \AndyTruong\Common\Traits\NameAwareTrait,
        \GO1\FormCenter\Entity\Type\EntityTypeTrait,
        \AndyTruong\Serializer\SerializableTrait;
}
