<?php

namespace GO1\FormCenter\Entity\Type;

use GO1\FormCenter\Entity\Storage\EntityStorageHandlerInterface;
use GO1\FormCenter\Field\FieldInterface;

interface EntityTypeInterface
{

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name);

    /**
     * @return string Machine name of entity type.
     */
    public function getName();

    /**
     * Set description.
     *
     * @param string $description
     */
    public function setDescription($description);

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set human name.
     *
     * @param string $name
     */
    public function setHumanName($name);

    /**
     * Get label.
     *
     * @return string Readable name.
     */
    public function getHumanName();

    /**
     * Set plural human name.
     *
     * @param string $name
     */
    public function setPluralHumanName($name);

    /**
     * Get plural human name.
     *
     * @return string
     */
    public function getPluralHumanName();

    /**
     * Set locked.
     *
     * @param bool $locked
     */
    public function setLocked($locked);

    /**
     * Is the entity-type locked.
     *
     * return @bool
     */
    public function isLocked();

    /**
     * Set storage handler.
     *
     * @param EntityStorageHandlerInterface $storage
     */
    public function setStorageHandler(EntityStorageHandlerInterface $storage);

    /**
     * Not all remote system use same id key, it can be: id, Id, nid, uid, …
     *
     * @param string $key
     */
    public function setIDKey($key);

    /**
     * Get ID key.
     *
     * @return string
     */
    public function getIDKey();

    /**
     * Get storage handler. This is quite strange here, by designed, entity types
     * may come from the standalone web application, but they can also be come
     * from external services, like Sales/Drupal/… In this case, we need storage
     * handler for each remote service.
     *
     * @return EntityStorageHandlerInterface
     */
    public function getStorageHandler();

    /**
     * Check existence of a field.
     *
     * @param string $fieldName
     * @return bool
     */
    public function hasField($fieldName);

    /**
     * Add a field to entity type.
     *
     * @param FieldInterface $field
     */
    public function addField(FieldInterface $field);

    /**
     * Remove a field from entity-type.
     *
     * @param string $field_name
     */
    public function removeField($field_name);

    /**
     * Get fields.
     *
     * @return FieldInterface[]
     *      Associated array of fields, for example: ['dob' => <DateField>Field]
     */
    public function getFields();

    /**
     * Get field by name.
     *
     * @param string $fieldName
     * @return FieldInterface
     */
    public function getField($fieldName);
}
