<?php

namespace GO1\FormCenter\Entity\Type;

use GO1\FormCenter\Entity\Storage\EntityStorageHandlerInterface;
use GO1\FormCenter\Field\FieldInterface;

trait EntityTypeTrait
{

    /** @var string */
    private $pluralHumanName;

    /** @var string */
    private $description;

    /** @var bool */
    private $locked;

    /** @var string */
    private $identifyKey;

    /** @var FieldInterface[] */
    private $fields;

    /* @var \GO1\FormCenter\Entity\Storage\EntityStorageHandlerInterface */
    private $storageHandler;

    /**
     * {@inheritdoc}
     * @param type $description
     * @return EntityTypeBase
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * {@inheritdoc}
     * @param string $label
     * @return EntityTypeBase
     */
    public function setPluralHumanName($label)
    {
        $this->pluralHumanName = $label;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getPluralHumanName()
    {
        return $this->pluralHumanName;
    }

    /**
     * {@inheritdoc}
     * @param bool $locked
     * @return EntityTypeBase
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function isLocked()
    {
        return true === $this->locked;
    }

    /**
     * {@inheritdoc}
     * @param string $key
     * @return EntityTypeBase
     */
    public function setIDKey($key)
    {
        $this->identifyKey = $key;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getIDKey()
    {
        return $this->identifyKey;
    }

    /**
     * {@inheritdoc}
     * @param EntityStorageHandlerInterface $storage
     */
    public function setStorageHandler(EntityStorageHandlerInterface $storage)
    {
        $this->storageHandler = $storage;
    }

    /**
     * {@inheritdoc}
     * @return EntityStorageHandlerInterface
     */
    public function getStorageHandler()
    {
        if (null === $this->storageHandler) {
            $this->setStorageHandler($this->getDefaultStorageHandler());
        }
        return $this->storageHandler;
    }

    /**
     * {@inheritdoc}
     * @param string $fieldName
     * @return bool
     */
    public function hasField($fieldName)
    {
        return isset($this->fields[$fieldName]);
    }

    /**
     * {@inheritdoc}
     * @param FieldInterface $field
     * @return EntityStorageHandlerInterface
     */
    public function addField(FieldInterface $field)
    {
        $this->fields[$field->getName()] = $field;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $field_name
     * @return EntityTypeBase
     */
    public function removeField($field_name)
    {
        unset($this->fields[$field_name]);
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return FieldInterface[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * {@inheritdoc}
     * @param string $fieldName
     * @return FieldInterface
     */
    public function getField($fieldName)
    {
        return isset($this->fields[$fieldName]) ? $this->fields[$fieldName] : null;
    }

}
