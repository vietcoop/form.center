<?php

namespace GO1\FormCenter\Entity;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldValueItemInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class EntityBase implements EntityInterface
{

    /** @var int */
    private $id;

    /** @var EntityTypeInterface */
    private $entityType;

    /** @var FieldValueItemInterface[] */
    private $fieldValueItems;

    /**
     *
     * @param type $id
     * @return EntityInterface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     * @param EntityTypeInterface $entityType
     * @return EntityInterface
     */
    public function setEntityType(EntityTypeInterface $entityType)
    {
        $this->entityType = $entityType;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return EntityTypeInterface
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * {@inheritdoc}
     * @param string $fieldName
     * @return FieldValueItemInterface[]
     */
    public function getFieldValueItems($fieldName)
    {
        return isset($this->fieldValueItems[$fieldName]) ? $this->fieldValueItems[$fieldName] : [];
    }

    /**
     * {@inheritdoc}
     * @param string $fieldName
     * @param FieldValueItemInterface $fieldValue
     * @param int $delta
     * @return EntityInterface
     */
    public function setFieldValueItem($fieldName, FieldValueItemInterface $fieldValue, $delta = 0)
    {
        if (null !== $delta) {
            $this->fieldValueItems[$fieldName][$delta] = $fieldValue;
        }
        else {
            $this->fieldValueItems[$fieldName][] = $fieldValue;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $fieldName
     * @param integer $delta
     * @return EntityInterface
     */
    public function removeFieldValueItem($fieldName, $delta)
    {
        unset($this->fieldValueItems[$fieldName][$delta]);
    }

    /**
     * {@inheritdoc}
     * @param string $fieldName
     * @param int $delta
     * @return FieldValueItemInterface
     */
    public function getFieldValueItem($fieldName, $delta = 0)
    {
        return $this->fieldValueItems[$fieldName][$delta];
    }

    /**
     * {@inheritdoc}
     * @return ConstraintViolationList
     */
    public function validate()
    {
        $errors = new ConstraintViolationList();
        foreach ($this->getEntityType()->getFields() as $fieldName => $field) {
            $_errors = $field->validate($this->getFieldValueItems($fieldName));
            if (count($_errors)) {
                $errors->addAll($_errors);
            }
        }
        return $errors;
    }

}
