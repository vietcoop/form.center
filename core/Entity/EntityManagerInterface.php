<?php

namespace GO1\FormCenter\Entity;

interface EntityManagerInterface
{

    /**
     * Get entity info by entity object.
     *
     * @param EntityInterface $entity
     */
    public function getInfoByEntity(EntityInterface $entity);

    /**
     * Get entity info by name of entity type.
     *
     * @param string $entity_type_name
     */
    public function getInfoByType($entity_type_name);

    /**
     * Get all entity info.
     */
    public function getAllInfo();

    /**
     * Get ID of entity object.
     *
     * @param EntityInterface $entity
     */
    public function getEntityId(EntityInterface $entity);

    /**
     * Load entity by ID.
     *
     * @param string $type
     * @param int|uuid $id
     * @return EntityInterface
     */
    public function load($type, $id);

    /**
     * Load multiple entities by ID.
     *
     * @param string $type
     * @param int[]|uuid[] $ids
     * @return EntityInterface[]
     */
    public function loadMultiple($type, array $ids);

    /**
     * Update entity interface.
     *
     * @param EntityInterface $entity
     */
    public function update(EntityInterface $entity);

    /**
     * Patch entity.
     *
     * @param string $type
     * @param int|uuid $id
     * @param array $commands
     */
    public function patch($type, $id, $commands);

    /**
     * Delete entity by ID.
     *
     * @param string $type
     * @param int|uuid $id
     */
    public function delete($type, $id);

    /**
     * Delete multiple entity by ID.
     *
     * @param string $type
     * @param int[]|uuid[] $ids
     */
    public function deleteMultiple($type, array $ids);
}
