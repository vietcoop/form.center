<?php

namespace GO1\FormCenter\Entity;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldValueItemInterface;
use Symfony\Component\Validator\ConstraintViolationList;

interface EntityInterface
{

    /**
     * Get entity ID.
     *
     * @return int|uuid
     */
    public function getId();

    /**
     * Set entity type.
     *
     * @param EntityTypeInterface $entityType
     * @throw \RuntimeException Do not allow change entity type.
     */
    public function setEntityType(EntityTypeInterface $entityType);

    /**
     * Get entity type.
     *
     * @return EntityTypeInterface
     */
    public function getEntityType();

    /**
     * Set field value.
     *
     * @param string $fieldName
     * @param FieldValueItemInterface $fieldValue
     * @param int $delta
     * @return EntityTypeInterface
     */
    public function setFieldValueItem($fieldName, FieldValueItemInterface $fieldValue, $delta = null);

    /**
     * Remove value-item from a field.
     *
     * @param int $delta
     */
    public function removeFieldValueItem($fieldName, $delta);

    /**
     * get all field value items.
     *
     * @param string $fieldName
     * @return FieldValueItemInterface[]
     */
    public function getFieldValueItems($fieldName);

    /**
     * Get value of a field.
     *
     * @param string $fieldName
     * @param int $delta
     * @return FieldValueItemInterface
     */
    public function getFieldValueItem($fieldName, $delta = 0);

    /**
     * Validate entity.
     *
     * @return ConstraintViolationList
     */
    public function validate();
}
