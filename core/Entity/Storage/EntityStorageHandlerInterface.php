<?php

namespace GO1\FormCenter\Entity\Storage;

use GO1\FormCenter\Entity\EntityInterface;

/**
 * Handler to store entity.
 */
interface EntityStorageHandlerInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Check supporting of an entity type.
     *
     * @param string $entityTypeName
     * @return bool
     */
    public function support($entityTypeName);

    /**
     * Create entity.
     *
     * @param EntityInterface $entity
     * @return int|string Entity ID/Uuid
     */
    public function create(EntityInterface $entity);

    /**
     * Update entity.
     *
     * @param EntityInterface $entity
     */
    public function update(EntityInterface $entity);

    /**
     * Patch entity.
     *
     * @param EntityInterface $entity
     * @param array $cmds
     */
    public function patch(EntityInterface $entity, array $cmds = []);

    /**
     * Delete an entity.
     *
     * @param EntityInterface $entity
     */
    public function delete(EntityInterface $entity);

    /**
     * Delete an entity by ID.
     *
     * @param string $entityTypeName
     * @param int $id
     */
    public function deleteById($entityTypeName, $id);
}
