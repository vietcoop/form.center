<?php

namespace GO1\FormCenter\Entity\Storage;

/**
 * @TODO: Complete me
 */
interface EntityStorageProviderInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Get entity storage handlers.
     *
     * @return EntityStorageHandlerInterface
     */
    public function getEntityStorageHandlers();
}
