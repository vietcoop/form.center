<?php

namespace GO1\FormCenter\Entity;

class EntityManagerBase implements EntityManagerInterface
{

    public function delete($type, $id)
    {

    }

    public function deleteMultiple($type, array $ids)
    {

    }

    public function getAllInfo()
    {

    }

    public function getEntityId(EntityInterface $entity)
    {

    }

    public function getInfoByEntity(EntityInterface $entity)
    {

    }

    public function getInfoByType($entity_type_name)
    {

    }

    public function load($type, $id)
    {

    }

    public function loadMultiple($type, array $ids)
    {

    }

    public function patch($type, $id, $commands)
    {

    }

    public function update(EntityInterface $entity)
    {

    }

}
