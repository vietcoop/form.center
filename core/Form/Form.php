<?php

namespace GO1\FormCenter\Form;

class Form implements FormInterface
{

    use \AndyTruong\Uuid\UuidGeneratorAwareTrait,
        \GO1\FormCenter\Form\FormTrait;
}
