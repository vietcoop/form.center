<?php

namespace GO1\FormCenter\Form;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetInterface;
use GO1\FormCenter\Form\Layout\FormLayoutInterface;
use GO1\FormCenter\Form\Layout\FormLayoutOptions;
use GO1\FormCenter\Form\Listener\FormListenerInterface;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;
use UnexpectedValueException;

trait FormTrait
{

    /** @var EntityTypeInterface */
    private $entityTypes = [];

    /** @var FormLayoutInterface */
    private $layout;

    /** @var FormLayoutOptions */
    private $layoutOptions;

    /** @var FieldInterface[] */
    private $fields = [];

    /** @var FieldWidgetInterface[] */
    private $fieldWidgets;

    /** @var string[] */
    private $entityTypeFields = [];

    /** @var FormListenerInterface[] */
    private $listeners = [];

    /** @var array */
    private $listenerOptions;

    /**
     * {@inheritdoc}
     * @param EntityTypeInterface $entityType
     */
    public function addEntityType(EntityTypeInterface $entityType)
    {
        $this->entityTypes[$entityType->getName()] = $entityType;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $entityTypeName
     */
    public function removeEntityType($entityTypeName)
    {
        unset($this->entityTypes[$entityTypeName]);
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $entityTypeName
     * @return EntityTypeInterface
     */
    public function getEntityType($entityTypeName)
    {
        return isset($this->entityTypes[$entityTypeName]) ? $this->entityTypes[$entityTypeName] : null;
    }

    /**
     * Get entity type by field-uuid.
     *
     * @param string $field_uuid
     * @return string
     */
    public function getEntityTypeNameByFieldUuid($field_uuid)
    {
        foreach ($this->entityTypeFields as $entity_type_uuid => $field_uuids) {
            if (in_array($field_uuid, $field_uuids)) {
                return $entity_type_uuid;
            }
        }
    }

    /**
     * {@inheritdoc}
     * @return EntityTypeInterface[]
     */
    public function getEntityTypes()
    {
        return $this->entityTypes;
    }

    /**
     * {@inheritdoc}
     * @param string $entityTypeName
     * @param FieldInterface $field
     */
    public function addField($entityTypeName, FieldInterface $field)
    {
        $key = $entityTypeName . '.' . $field->getName();
        $this->fields[$key] = $field;
        return $key;
    }

    /**
     * {@inheritdoc}
     * @param string $fieldKey
     * @param FieldWidgetInterface $fieldWidget
     */
    public function setFieldWidget($fieldKey, FieldWidgetInterface $fieldWidget)
    {
        if (!$fieldWidget->supports($this->getFieldByKey($fieldKey)->getFieldType()->getName())) {
            throw new UnexpectedValueException(strtr(':widget is not compatible with :field', [
                ':widget' => $fieldWidget->getHumanName(),
                ':field'  => $this->getFieldByKey($fieldKey)->getFieldType()->getHumanName()
            ]));
        }
        $this->fieldWidgets[$fieldKey] = $fieldWidget;
    }

    /**
     * {@inheritdoc}
     * @param string $fieldUuid
     * @return FieldWidgetInterface
     */
    public function getFieldWidget($fieldUuid)
    {
        if (isset($this->fieldWidgets[$fieldUuid])) {
            return $this->fieldWidgets[$fieldUuid];
        }
        return $this->getField($fieldUuid)->getFieldWidget();
    }

    /**
     * {@inheritdoc}
     * @param string $fieldUuid
     * @return FieldInterface|null
     */
    public function getField($entityTypeName, $fieldName)
    {
        return $this->getFieldByKey("{$entityTypeName}.{$fieldName}");
    }

    public function getFieldByKey($key)
    {
        return isset($this->fields[$key]) ? $this->fields[$key] : null;
    }

    /**
     * {@inheritdoc}
     * @param string $field_uuid
     */
    public function removeField($field_uuid)
    {
        // Remove from fields property
        unset($this->fields[$field_uuid]);

        // Remove from entityTypeFields property
        foreach ($this->entityTypeFields as $entity_type_uuid => $field_uuids) {
            foreach ($field_uuids as $i => $_field_uuid) {
                if ($field_uuid === $_field_uuid) {
                    unset($this->entityTypeFields[$entity_type_uuid][$i]);
                    break;
                }
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     * @return FieldInterface[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * {@inheritdoc}
     * @param FormListenerInterface $listener
     * @param array $options
     * @return string
     */
    public function addListener(FormListenerInterface $listener, array $options = [])
    {
        $uuid = $this->getUuidGenerator()->generate();
        $this->listeners[$uuid] = $listener;
        $this->listenerOptions[$uuid] = $options;
        return $uuid;
    }

    /**
     * {@inheritdoc}
     * @param int $uuid
     */
    public function removeListener($uuid)
    {
        unset($this->listeners[$uuid]);
        unset($this->listenerOptions[$uuid]);
    }

    /**
     * {@inheritdoc}
     * @return FormListenerInterface[]
     */
    public function getListeners()
    {
        return $this->listeners;
    }

    /**
     * {@inheritdoc}
     * @param string $uuid
     * @return FormListenerInterface
     */
    public function getListener($uuid)
    {
        return isset($this->listeners[$uuid]) ? $this->listeners[$uuid] : null;
    }

    /**
     * {@inheritdoc}
     * @param string $uuid
     * @return array
     */
    public function getListenerOptions($uuid)
    {
        return isset($this->listenerOptions[$uuid]) ? $this->listenerOptions[$uuid] : null;
    }

    /**
     * {@inheritdoc}
     * @param FormLayoutInterface $layout
     */
    public function setLayout(FormLayoutInterface $layout)
    {
        $this->layout = $layout;
    }

    /**
     * {@inheritdoc}
     * @return FormLayoutInterface
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function hasLayoutOptions()
    {
        return null !== $this->layoutOptions;
    }

    /**
     * {@inheritdoc}
     * @param FormLayoutOptions $layoutOptions
     */
    public function setLayoutOptions(FormLayoutOptions $layoutOptions)
    {
        $this->layoutOptions = $layoutOptions;
    }

    /**
     * {@inheritdoc}
     * @return FormLayoutOptions
     */
    public function getLayoutOptions()
    {
        return $this->layoutOptions;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function render($pageNumber = 1, FormSubmissionInterface $submission = null)
    {
        $output = $this->getLayout()->render($this, $this->getLayoutOptions(), $pageNumber, $submission);
        $this->trigger('onRender', ['output' => $output, 'page' => $pageNumber]);
        return $output;
    }

    public function trigger($eventName, $params)
    {
        foreach ($this->getListeners() as $listener) {
            $listener->{$eventName}($this, $params);
        }
    }

}
