<?php

namespace GO1\FormCenter\Form\Submission;

use GO1\FormCenter\Entity\EntityInterface;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldValueItem;
use GO1\FormCenter\Field\FieldValueItemInterface;
use GO1\FormCenter\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * A form submission can be collection of one or multiple entities.
 */
interface FormSubmissionInterface
{

    /**
     * Inject form object.
     *
     * @param FormInterface $form
     */
    public function setForm(FormInterface $form);

    /**
     * Get form.
     *
     * @return FormInterface
     */
    public function getForm();

    /**
     * Set field input.
     * @param string $entityTypeName
     * @param string $fieldName
     * @param FieldValueItemInterface $fieldValue
     * @param int $delta
     */
    public function setFieldInput($entityTypeName, $fieldName, FieldValueItemInterface $fieldValue, $delta = 0);

    /**
     * @return FieldValueItem[]
     * @param string $entityTypeName
     * @param string $fieldName
     */
    public function getFieldValues($entityTypeName, $fieldName);

    /**
     * Inject an entity to form submission.
     *
     * @param string $entityTypeName
     * @param EntityInterface $entity
     * @return FormSubmissionInterface
     */
    public function setEntity($entityTypeName, EntityInterface $entity);

    /**
     * Get entity by entity type ID.
     *
     * @param string $entityTypeName
     * @return EntityInterface
     */
    public function getEntity($entityTypeName);

    /**
     * @return EntityTypeInterface[]
     *      Associated array — [EntityTypeName => Entity]
     */
    public function getEntities();

    /**
     * Validate user input.
     *
     * @return ConstraintViolationList
     */
    public function validate($pageNumber = 1);
}
