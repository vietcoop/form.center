<?php

namespace GO1\FormCenter\Form\Submission;

use GO1\FormCenter\Entity\EntityInterface;
use GO1\FormCenter\Field\FieldValueItemInterface;
use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;
use GO1\FormCenter\Manager\Manager;
use Symfony\Component\Validator\ConstraintViolationList;

class FormSubmission implements FormSubmissionInterface
{

    use \AndyTruong\Event\EventAwareTrait;

    /** @var Manager */
    private $manager;

    /** @var FormInterface */
    private $form;

    /** @var EntityInterface[] */
    private $entities = [];

    /**
     * Constructor.
     *
     * @param Manager $manager
     * @param FormInterface $form
     */
    public function __construct(Manager $manager, FormInterface $form)
    {
        $this->manager = $manager;
        $this->setForm($form);
    }

    /**
     * {@inheritdoc}
     * @param FormInterface $form
     */
    public function setForm(FormInterface $form)
    {
        $this->form = $form;
    }

    /**
     * {@inheritdoc}
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * {@inheritdoc}
     * @param string $entityTypeName
     * @param string $fieldName
     * @param FieldValueItemInterface $fieldValue
     * @param int $delta
     */
    public function setFieldInput($entityTypeName, $fieldName, FieldValueItemInterface $fieldValue, $delta = 0)
    {
        $this
            ->getEntity($entityTypeName)
            ->setFieldValueItem($fieldName, $fieldValue, $delta);
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldValues($entityTypeName, $fieldName)
    {
        return $this
                ->getEntity($entityTypeName)
                ->getFieldValueItems($fieldName);
    }

    /**
     * {@inheritdoc}
     * @return EntityInterface
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * {@inheritdoc}
     * @param string $entityTypeName
     * @param EntityInterface $entity
     * @return FormSubmission
     */
    public function setEntity($entityTypeName, EntityInterface $entity)
    {
        $this->entities[$entityTypeName] = $entity;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $entityTypeName
     */
    public function getEntity($entityTypeName)
    {
        if (!isset($this->entities[$entityTypeName])) {
            $entityType = $this->getForm()->getEntityType($entityTypeName);
            $this->entities[$entityTypeName] = $this->manager->createEntity($entityType);
        }
        return $this->entities[$entityTypeName];
    }

    /**
     * {@inheritdoc}
     * @return ConstraintViolationList
     */
    public function validate($pageNumber = 1)
    {
        $errors = new ConstraintViolationList();
        $pageOptions = $this->getForm()->getLayoutOptions()->getPageByNumber($pageNumber);

        if (empty($pageOptions['fields'])) {
            return $errors;
        }

        foreach (array_keys($pageOptions['fields']) as $fieldKey) {
            $field = $this->getForm()->getFieldByKey($fieldKey);
            $fieldValueItems = $this->getFieldValues($field->getEntityType()->getName(), $field->getName());
            if ($subErrors = $field->validate($fieldValueItems)) {
                $errors->addAll($subErrors);
            }
        }
        return $errors;
    }

}
