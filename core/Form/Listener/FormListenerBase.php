<?php

namespace GO1\FormCenter\Form\Listener;

use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;
use Symfony\Component\Validator\ConstraintViolationList;

abstract class FormListenerBase implements FormListenerInterface
{

    use \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var FormInterface  */
    private $form;

    /** @var bool */
    protected $wildcard = false;

    /**
     * Inject form the form
     *
     * @param FormInterface $form
     */
    public function setForm(FormInterface $form)
    {
        $this->form = $form;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    public function onRender(FormInterface $form, array $params)
    {
        if ($this->wildcard) {
            return $this->onWildcard(__FUNCTION__, $form, $params);
        }
    }

    public function onValidate(FormSubmissionInterface $submission)
    {
        if ($this->wildcard) {
            return $this->onWildcard(__FUNCTION__, $submission);
        }
    }

    public function onFailedValidation(ConstraintViolationList $errors)
    {
        if ($this->wildcard) {
            return $this->onWildcard(__FUNCTION__, $errors);
        }
    }

    public function onSuccessValidation(ConstraintViolationList $errors)
    {
        if ($this->wildcard) {
            return $this->onWildcard(__FUNCTION__, $errors);
        }
    }

    public function onBeforeFormSubmit(FormSubmissionInterface $submission)
    {
        if ($this->wildcard) {
            return $this->onWildcard(__FUNCTION__, $submission);
        }
    }

    public function onAfterFormSubmit(FormSubmissionInterface $submission)
    {
        if ($this->wildcard) {
            return $this->onWildcard(__FUNCTION__, $submission);
        }
    }

    public function onWildcard()
    {

    }

}
