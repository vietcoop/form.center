<?php

namespace GO1\FormCenter\Form\Listener;

use AndyTruong\Event\DispatcherAwareInterface;

interface FormListenerProviderInterface extends DispatcherAwareInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * @return FormListenerInterface[]
     */
    public function getFormListeners();
}
