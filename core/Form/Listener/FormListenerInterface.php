<?php

namespace GO1\FormCenter\Form\Listener;

use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;
use Symfony\Component\Validator\ConstraintViolationList;

interface FormListenerInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Inject form object.
     *
     * @param FormInterface $form
     */
    public function setForm(FormInterface $form);

    /**
     * @return FormInterface
     */
    public function getForm();

    /**
     * To be called on form render.
     */
    public function onRender(FormInterface $form, array $params);

    /**
     * To be called on validate form submission.
     *
     * @param FormSubmissionInterface $submission
     */
    public function onValidate(FormSubmissionInterface $submission);

    /**
     * If submission does not pass the form validation, this method will be executed.
     *
     * @param ConstraintViolationList $errors
     */
    public function onFailedValidation(ConstraintViolationList $errors);

    /**
     * If submission passes the form validation, this method will be executed.
     *
     * @param ConstraintViolationList $errors
     */
    public function onSuccessValidation(ConstraintViolationList $errors);

    /**
     * To be called just before the form submitted.
     *
     * @param FormSubmissionInterface $submission
     */
    public function onBeforeFormSubmit(FormSubmissionInterface $submission);

    /**
     * To be called after the form submitted.
     *
     * @param FormSubmissionInterface $submission
     */
    public function onAfterFormSubmit(FormSubmissionInterface $submission);
}
