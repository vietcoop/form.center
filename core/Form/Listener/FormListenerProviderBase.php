<?php

namespace GO1\FormCenter\Form\Listener;

class FormListenerProviderBase implements FormListenerProviderInterface
{

    use \AndyTruong\Event\EventAwareTrait,
        \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var FormListenerInterface[] */
    private $formListeners = [];

    public function getFormListeners()
    {
        return $this->formListeners;
    }

}
