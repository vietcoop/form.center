<?php

namespace GO1\FormCenter\Form\Layout;

class FieldGroup
{

    /** @var string */
    private $title;

    /** @var string */
    private $type;

    /** @var string */
    private $description;

    /** var array */
    private $options;

    /** @var string */
    private $parent;

    /** @var string */
    private $weight;

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
    }

    public function getOption($name, $defaultValue = null)
    {
        return isset($this->options[$name]) ? $this->options[$name] : $defaultValue;
    }

    public function getOptions()
    {
        return $this->options;
    }

}
