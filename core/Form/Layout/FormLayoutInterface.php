<?php

namespace GO1\FormCenter\Form\Layout;

use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;

interface FormLayoutInterface
{

    /**
     * Get layout's machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get layout's human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Get path to layout's screenshot.
     *
     * @return string
     */
    public function getScreenshot();

    /**
     * Render the form.
     *
     * @param FormInterface $form
     * @param FormLayoutOptions $options
     * @param int $page
     * @param FormSubmissionInterface $submission
     */
    public function render(FormInterface $form, FormLayoutOptions $options, $page = 1, FormSubmissionInterface $submission = null);
}
