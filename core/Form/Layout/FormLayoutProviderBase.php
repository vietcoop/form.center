<?php

namespace GO1\FormCenter\Form\Layout;

abstract class FormLayoutProviderBase implements FormLayoutProviderInterface
{

    use \AndyTruong\Common\Traits\NameAwareTrait;

    private $formLayouts = [];

    /**
     * {@inheritdoc}
     * @param FormLayoutInterface $layout
     * @return self
     */
    public function registerFormLayout(FormLayoutInterface $layout)
    {
        $this->formLayouts[$layout->getName()] = $layout;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $name
     * @return self
     */
    public function unregisterFormLayout($name)
    {
        unset($this->formLayouts[$name]);
        return $this;
    }

    public function getFormLayouts()
    {
        return $this->formLayouts;
    }

    public function getFormLayout($name)
    {
        return isset($this->formLayouts[$name]) ? $this->formLayouts[$name] : null;
    }

}
