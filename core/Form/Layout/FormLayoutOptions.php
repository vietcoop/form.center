<?php

namespace GO1\FormCenter\Form\Layout;

use GO1\FormCenter\Field\FieldOptions;
use RuntimeException;

/**
 * Options for form layout rendering, is attached with form.
 */
class FormLayoutOptions
{

    use \AndyTruong\Uuid\UuidGeneratorAwareTrait;

    /** @var string */
    private $submitText = 'Submit';

    /** @var string */
    private $confirmationMessage = 'Thank you for your submission.';

    /** @var array */
    private $pageDivisions = [];

    private function getHeaviestValue($items)
    {
        $max = 0;
        foreach ($items as $item) {
            $max = max([$max, isset($item['weight']) ? $item['weight'] : 0]);
        }
        return $max;
    }

    public function setSubmitText($submitText)
    {
        $this->submitText = $submitText;
        return $this;
    }

    public function getSubmitText()
    {
        return $this->submitText;
    }

    public function setConfirmationMessage($confirmationMessage)
    {
        $this->confirmationMessage = $confirmationMessage;
        return $this;
    }

    public function getConfirmationMessage()
    {
        return $this->confirmationMessage;
    }

    /**
     * Add new page.
     *
     * @param string $pageUuid
     * @param string $title
     * @param string $description
     * @param string $help
     * @param int $weight
     * @return string
     */
    public function addPage($pageUuid = null, $title = null, $description = null, $help = null, $weight = null)
    {
        if (null === $pageUuid) {
            $pageUuid = $this->getUuidGenerator()->generate();
        }

        $this->pageDivisions[$pageUuid] = [
            'title'       => $title,
            'description' => $description,
            'help'        => $help,
            'weight'      => (null !== $weight) ? $weight : 1 + $this->getHeaviestValue($this->pageDivisions),
            'fields'      => [],
        ];
        return $pageUuid;
    }

    /**
     * Get page.
     *
     * @param string $uuid
     */
    public function getPage($uuid)
    {
        return isset($this->pageDivisions[$uuid]) ? $this->pageDivisions[$uuid] : null;
    }

    /**
     * Get all pages.
     *
     * @return array
     */
    public function getPages()
    {
        return $this->pageDivisions;
    }

    private function getSortedPageUuids()
    {
        uksort($this->pageDivisions, function($uuidA, $uuidB) {
            return ($this->pageDivisions[$uuidA]['weight'] < $this->pageDivisions[$uuidB]['weight']) ? -1 : 1;
        });
        return array_keys($this->pageDivisions);
    }

    /**
     * Get page by number.
     *
     * @param int $pageNumber
     * @return array
     */
    public function getPageByNumber($pageNumber = 1)
    {
        if (isset($this->pageDivisions[$pageNumber])) {
            return $this->pageDivisions[$pageNumber];
        }
        $uuid = $this->getSortedPageUuids()[$pageNumber - 1];
        return $this->pageDivisions[$uuid];
    }

    public function getPreviousPage($pageNumber = 1)
    {
        return $pageNumber > 1 ? $pageNumber - 1 : false;
    }

    public function getNextPage($pageNumber = 1)
    {
        return $pageNumber < count($this->pageDivisions) ? $pageNumber + 1 : false;
    }

    public function isLastPage($pageNumber = 1)
    {
        return false === $this->getNextPage($pageNumber);
    }

    /**
     * Remove a page.
     *
     * @param string $pageUuid
     * @param string $newFieldPageUuid
     */
    public function removePage($pageUuid, $newFieldPageUuid = null)
    {
        if (null !== $newFieldPageUuid) {
            foreach (array_keys($this->pageDivisions[$pageUuid]['fields']) as $fieldUuid) {
                $this->addField($newFieldPageUuid, $fieldUuid);
            }
        }
        unset($this->pageDivisions[$pageUuid]);
    }

    /**
     * Add field to form/page/group.
     *
     * @param string $pageUuid
     * @param string $fieldKey
     * @param string $parent
     * @param int $weight
     * @param strring $domTagName
     * @param string $domId
     * @param string[] $domClasses
     * @return FormLayoutOptions
     */
    public function addField($pageUuid, $fieldKey, $parent = null, $weight = null, $domTagName = 'div', $domId = null, array $domClasses = [])
    {
        $this->pageDivisions[$pageUuid]['fields'][$fieldKey] = new FieldOptions([
            'parent'     => $parent,
            'weight'     => null !== $weight ? $weight : 1 + $this->getHeaviestValue($this->pageDivisions[$pageUuid]),
            'domTagName' => $domTagName,
            'domId'      => $domId,
            'domClasses' => $domClasses,
        ]);
        return $this;
    }

    /**
     * Update a field option.
     *
     * @param string $fieldKey
     * @param string $name
     * @param mixed $value
     */
    public function setFieldOption($fieldKey, $name, $value)
    {
        foreach ($this->pageDivisions as $pageUuid => $pageInfo) {
            if (isset($pageInfo['fields'][$fieldKey])) {
                $fieldInfo = $pageInfo['fields'][$fieldKey];
                break;
            }
        }

        if (empty($fieldInfo) || !isset($fieldInfo[$name])) {
            throw new RuntimeException('Invalid field option.');
        }

        $this->pageDivisions[$pageUuid]['fields'][$fieldKey][$name] = $value;
    }

    /**
     * @param string $fieldKey
     * @return FieldOptions
     */
    public function getFieldOptions($fieldKey)
    {
        foreach ($this->pageDivisions as $pageInfo) {
            if (isset($pageInfo['fields'][$fieldKey])) {
                return $pageInfo['fields'][$fieldKey];
            }
        }
    }

    public function getFieldWeight($fieldUuid)
    {
        if ($fieldOptions = $this->getFieldOptions($fieldUuid)) {
            return $fieldOptions->getWeight();
        }
        return 0;
    }

    public function addGroup(FieldGroup $group, $pageUuid, $groupUuid = null)
    {
        if (null === $groupUuid) {
            $groupUuid = $this->getUuidGenerator()->generate();
        }
        $this->pageDivisions[$pageUuid]['groups'][$groupUuid] = $group;
        return $this;
    }

    public function removeGroup($pageUuid, $groupUuid)
    {
        if (isset($this->pageDivisions[$pageUuid]['groups'][$groupUuid])) {
            unset($this->pageDivisions[$pageUuid]['groups'][$groupUuid]);
        }
        return $this;
    }

    /**
     * @param string $pageUuid
     * @return FieldGroup[]
     */
    public function getGroups($pageUuid)
    {
        if (isset($this->pageDivisions[$pageUuid]['groups'])) {
            return $this->pageDivisions[$pageUuid]['groups'];
        }
        return [];
    }

    /**
     * @param string $pageUuid
     * @param string $groupUuid
     * @return FieldGroup
     */
    public function getGroup($pageUuid, $groupUuid)
    {
        if (isset($this->pageDivisions[$pageUuid]['groups'][$groupUuid])) {
            return $this->pageDivisions[$pageUuid]['groups'][$groupUuid];
        }
        return null;
    }

}
