<?php

namespace GO1\FormCenter\Form\Layout;

interface FormLayoutProviderInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Register a layout with provider.
     *
     * @param FormLayoutInterface $layout
     */
    public function registerFormLayout(FormLayoutInterface $layout);

    /**
     * Unregister form layout by name
     *
     * @param string $name
     */
    public function unregisterFormLayout($name);

    /**
     * Get form layouts.
     *
     * @return FormLayoutInterface[]
     */
    public function getFormLayouts();

    /**
     * Get a form layout by name.
     *
     * @param string $name
     */
    public function getFormLayout($name);
}
