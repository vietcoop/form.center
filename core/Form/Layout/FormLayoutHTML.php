<?php

namespace GO1\FormCenter\Form\Layout;

use AndyTruong\Serializer\Serializer;
use GO1\FormCenter\Field\FieldOptions;
use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Layout\FormLayoutInterface;
use GO1\FormCenter\Form\Layout\FormLayoutOptions;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;

abstract class FormLayoutHTML implements FormLayoutInterface
{

    use \AndyTruong\Common\Traits\TemplateEngineAwareTrait,
        \AndyTruong\Common\Traits\NameAwareTrait,
        \AndyTruong\Uuid\UuidGeneratorAwareTrait;

    /** @var string Path to screenshot. */
    protected $screenshot;

    /** @var string Path to template. */
    protected $templatePath;

    public function __construct()
    {
        $this->setName('html');
        $this->setHumanName('HTML Layout');
    }

    /**
     * Inject template path.
     *
     * @param string $templatePath
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;
    }

    /**
     * Get template path.
     *
     * @return string
     */
    public function getTemplatePath()
    {
        if (null === $this->templatePath) {
            $this->setTemplatePath($this->getDefaultTemplatePath());
        }
        return $this->templatePath;
    }

    /**
     * Get default path to form template.
     *
     * @return string
     */
    protected function getDefaultTemplatePath()
    {
        return 'form.html.php';
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getScreenshot()
    {
        return $this->screenshot;
    }

    public function getMethod(FormInterface $form)
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     * @param FormInterface $form
     * @param FormLayoutOptions $options
     * @param int $pageNumber
     * @param FormSubmissionInterface $submission
     */
    public function render(FormInterface $form, FormLayoutOptions $options, $pageNumber = 1, FormSubmissionInterface $submission = null)
    {
        $serializer = new Serializer();
        $pageOptions = $options->getPageByNumber($pageNumber);

        $params = [
            'method'      => $this->getMethod($form),
            'action'      => $this->getAction($form),
            'token'       => $this->getToken($form, $pageNumber),
            'page'        => $pageNumber,
            'title'       => $pageOptions['title'],
            'description' => $pageOptions['description'],
            'help'        => $pageOptions['help'],
            'weight'      => $pageOptions['weight'],
            'pager'       => $this->getPager($form, $pageNumber),
            'buttons'     => $this->getFormButons($form, $pageNumber),
            'positions'   => [],
            'groups'      => [],
        ];

        // Groups
        $groups = $form->getLayoutOptions()->getPageByNumber($pageNumber)['groups'];
        foreach (isset($groups) ? $groups : [] as $groupId => $fieldGroup) {
            /* @var $fieldGroup FieldGroup */
            $params['positions'][$fieldGroup->getParent()][] = ['id' => $groupId, 'weight' => $fieldGroup->getWeight()];
            $params['groups'][$groupId] = $serializer->toArray($fieldGroup);
        }

        uksort($params['groups'], function($a, $b) use ($params) {
            return $params['groups'][$a]['weight'] < $params['groups'][$b]['weight'] ? -1 : 1;
        });

        // Fields
        uksort($pageOptions['fields'], function($a, $b) use ($pageOptions) {
            return $pageOptions['fields'][$a]->getWeight() < $pageOptions['fields'][$b]->getWeight() ? -1 : 1;
        });

        foreach ($pageOptions['fields'] as $fieldKey => $fieldOptions) {
            /* @var $fieldOptions FieldOptions */
            $field = $form->getFieldByKey($fieldKey);
            $entityTypeName = $field->getEntityType()->getName();
            $fieldValues = [];
            if (null !== $submission) {
                $fieldValues = $submission->getFieldValues($entityTypeName, $field->getName());
            }

            $params['positions'][$fieldOptions->getParent()][] = ['id' => $fieldKey, 'weight' => $fieldOptions->getWeight()];
            $params['fields'][$fieldKey] = $field->render($fieldOptions, $fieldValues);
        }

        return $this->doRender($params);
    }

    protected function doRender($params)
    {
        return $this->getTemplateEngine()->render($this->getTemplatePath(), $params);
    }

}
