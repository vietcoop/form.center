<?php

namespace GO1\FormCenter\Form;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetInterface;
use GO1\FormCenter\Form\Layout\FormLayoutInterface;
use GO1\FormCenter\Form\Layout\FormLayoutOptions;
use GO1\FormCenter\Form\Listener\FormListenerInterface;
use GO1\FormCenter\Form\Submission\FormSubmissionInterface;

interface FormInterface
{

    /**
     * @return EntityTypeInterface[]
     */
    public function getEntityTypes();

    /**
     * Add an entity type to form.
     *
     * @param EntityTypeInterface $entityType
     */
    public function addEntityType(EntityTypeInterface $entityType);

    /**
     * Get entity type.
     *
     * @param string $entityTypeName
     */
    public function getEntityType($entityTypeName);

    /**
     * Get entity type by field UUID.
     *
     * @param string $fieldUuid
     * @return string
     */
    public function getEntityTypeNameByFieldUuid($fieldUuid);

    /**
     * Remove an entity type from form.
     *
     * @param string $entityTypeName
     */
    public function removeEntityType($entityTypeName);

    /**
     * Set form layout.
     *
     * @param FormLayoutInterface $layout
     */
    public function setLayout(FormLayoutInterface $layout);

    /**
     * Check available of layoutOptions property.
     *
     * @return bool
     */
    public function hasLayoutOptions();

    /**
     * Set layout options.
     *
     * @param FormLayoutOptions $options
     */
    public function setLayoutOptions(FormLayoutOptions $options);

    /**
     * Get options to render layout.
     *
     * @return FormLayoutOptions
     */
    public function getLayoutOptions();

    /**
     * Get layout.
     *
     * @return FormLayoutInterface
     */
    public function getLayout();

    /**
     * Add a field to form.
     *
     * @param string $entityTypeName
     * @param FieldInterface $field
     */
    public function addField($entityTypeName, FieldInterface $field);

    /**
     * EntityType already defines default widget for its fields. At form level,
     * we should provide an option for user to change default field's widget.
     *
     * For example, PersonEntity (an entity type) has a picture field. This
     * field use classic upload field. When use adds this field to form, he
     * can also change the widget to 'dropbox'.
     *
     * @param string $fieldUuid
     * @param FieldWidgetInterface $fieldWidget
     */
    public function setFieldWidget($fieldUuid, FieldWidgetInterface $fieldWidget);

    /**
     * Get field widget of a field.
     *
     * @param string $fieldUuid
     */
    public function getFieldWidget($fieldUuid);

    /**
     * Remove a field from form.
     *
     * @param string $fieldUuid
     */
    public function removeField($fieldUuid);

    /**
     * Get fields.
     *
     * @return FieldInterface[]
     */
    public function getFields();

    /**
     * Get a field by entity type name & field name.
     *
     * @param string $entityTypeName
     * @param string $fieldName
     * @return FieldInterface
     */
    public function getField($entityTypeName, $fieldName);

    /**
     * Get a field by key.
     *
     * @param string $fieldKey
     * @return FieldInterface
     */
    public function getFieldByKey($fieldKey);

    /**
     * Get listeners.
     *
     * @return FormListenerInterface[]
     */
    public function getListeners();

    /**
     * Get listener by id.
     *
     * @param string $uuid
     */
    public function getListener($uuid);

    /**
     * Get listeners options by id.
     *
     * @param string $uuid
     */
    public function getListenerOptions($uuid);

    /**
     * Add listener to form.
     *
     * @param FormListenerInterface $listener
     * @param array $options
     */
    public function addListener(FormListenerInterface $listener, array $options = []);

    /**
     * Remove a listener from form.
     *
     * @param string $uuid
     */
    public function removeListener($uuid);

    /**
     * Render the form with configured values.
     *
     * @param int $pageNumber
     * @param FormSubmissionInterface $submission
     */
    public function render($pageNumber = 1, FormSubmissionInterface $submission = null);
}
