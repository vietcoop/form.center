<?php

namespace GO1\FormCenter\Manager;

use AndyTruong\Event\DispatcherAwareInterface;
use GO1\FormCenter\Entity\Provider\EntityTypeProviderInterface;
use GO1\FormCenter\Entity\Storage\EntityStorageHandlerInterface;
use GO1\FormCenter\Entity\Storage\EntityStorageProviderInterface;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldTypeProviderInterface;
use GO1\FormCenter\Field\FieldWidgetProviderInterface;
use GO1\FormCenter\Field\Type\FieldTypeInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetBase;
use GO1\FormCenter\Field\Widget\FieldWidgetInterface;
use GO1\FormCenter\Form\Layout\FormLayoutInterface;
use GO1\FormCenter\Form\Layout\FormLayoutOptions;
use GO1\FormCenter\Form\Layout\FormLayoutProviderInterface;
use GO1\FormCenter\Form\Listener\FormListenerInterface;
use GO1\FormCenter\Form\Listener\FormListenerProviderInterface;

trait ProviderAwareManagerTrait
{

    /** @var EntityTypeProviderInterface Entity type providers. */
    private $entityTypeProviders = [];

    /** @var EntityTypeInterface[] */
    protected $entityTypes;

    /** @var FieldWidgetProviderInterface[] Widget providers. */
    private $fieldWidgetProviders = [];

    /** @var FieldWidgetInterface[] */
    private $fieldWidgets;

    /** @var FieldTypeProviderInterface[] Field type providers */
    private $fieldTypeProviders = [];

    /** @var FieldTypeInterface[] */
    protected $fieldTypes;

    /** @var FormLayoutProviderInterface[] */
    private $formLayoutProviders = [];

    /** @var FormLayoutInterface[] */
    private $formLayouts;

    /** @var FormListenerInterface[] Form listener providers */
    private $formListenerProviders;

    /** @var EntityStorageProviderInterface[] */
    private $entityStorageProviders = [];

    /** @var EntityStorageHandlerInterface[] */
    protected $entityStorageHandlers;

    /**
     * @return EntityTypeProviderInterface[]
     */
    public function getEntityTypeProviders()
    {
        return $this->entityTypeProviders;
    }

    /**
     * Register a entity-type provider to the manager.
     *
     * @param EntityTypeProviderInterface $provider
     */
    public function registerEntityTypeProvider(EntityTypeProviderInterface $provider)
    {
        if ($provider instanceof DispatcherAwareInterface) {
            $provider->setDispatcher($this->getDispatcher());
        }
        $this->entityTypeProviders[$provider->getName()] = $provider;
    }

    /**
     * Get available entity types.
     *
     * @return EntityTypeInterface[]
     */
    public function getEntityTypes()
    {
        if (null === $this->entityTypes) {
            $this->entityTypes = [];
            foreach ($this->getEntityTypeProviders() as $provider) {
                foreach ($provider->getEntityTypes() as $entityType) {
                    $name = $entityType->getName();
                    $this->entityTypes[$name] = $entityType;
                }
            }
        }

        return $this->entityTypes;
    }

    /**
     * Get entity-type by name.
     *
     * @param string $entityTypeName
     * @return EntityTypeInterface
     */
    public function getEntityType($entityTypeName)
    {
        $entityTypes = $this->getEntityTypes();
        return isset($entityTypes[$entityTypeName]) ? $entityTypes[$entityTypeName] : null;
    }

    /**
     * Get entity-type provider by name.
     *
     * @param string $name
     * @return EntityTypeProviderInterface
     */
    public function getEntityTypeProvider($name)
    {
        $entityTypeProviders = $this->getEntityTypeProviders();
        return isset($entityTypeProviders[$name]) ? $entityTypeProviders[$name] : null;
    }

    /**
     * @return FieldTypeProviderInterface[]
     */
    public function getFieldTypeProviders()
    {
        return $this->fieldTypeProviders;
    }

    /**
     * Register a field-type provider to the manager.
     *
     * @param FieldTypeProviderInterface $provider
     */
    public function registerFieldTypeProvider(FieldTypeProviderInterface $provider)
    {
        if ($provider instanceof DispatcherAwareInterface) {
            $provider->setDispatcher($this->getDispatcher());
        }
        $this->fieldTypeProviders[$provider->getName()] = $provider;
    }

    /**
     * Get field type provider by name.
     *
     * @param string $name
     * @return FieldTypeProviderInterface
     */
    public function getFieldTypeProvider($name)
    {
        $fieldTypeProviders = $this->getFieldTypeProviders();
        return isset($fieldTypeProviders[$name]) ? $fieldTypeProviders[$name] : null;
    }

    /**
     * Get available field types.
     *
     * @return FieldTypeInterface[]
     */
    public function getFieldTypes()
    {
        if (null === $this->fieldTypes) {
            foreach ($this->getFieldTypeProviders() as $provider) {
                foreach ($provider->getFieldTypes() as $fieldType) {
                    $name = $fieldType->getName();
                    $this->fieldTypes[$name] = $fieldType;
                }
            }
        }

        return $this->fieldTypes;
    }

    /**
     * Get field type by name.
     *
     * @param string $name
     * @return FieldTypeInterface
     */
    public function getFieldType($name)
    {
        $fieldTypes = $this->getFieldTypes();
        return isset($fieldTypes[$name]) ? $fieldTypes[$name] : null;
    }

    /**
     * @return FieldWidgetProviderInterface[]
     */
    public function getFieldWidgetProviders()
    {
        return $this->fieldWidgetProviders;
    }

    /**
     * Register a field-widget provider to the manager.
     *
     * @param FieldWidgetProviderInterface $provider
     */
    public function registerFieldWidgetProvider(FieldWidgetProviderInterface $provider)
    {
        if ($provider instanceof DispatcherAwareInterface) {
            $provider->setDispatcher($this->getDispatcher());
        }
        $this->fieldWidgetProviders[$provider->getName()] = $provider;
    }

    /**
     * Get field widget provider by name.
     *
     * @param string $name
     * @return FieldWidgetProviderInterface
     */
    public function getFieldWidgetProvider($name)
    {
        $fieldWidgetProviders = $this->getFieldWidgetProviders();
        return isset($fieldWidgetProviders[$name]) ? $fieldWidgetProviders[$name] : null;
    }

    /**
     * Get field widgets, limited by name of field type.
     *
     * @param string $fieldTypeName
     * @return FieldWidgetInterface[]
     */
    public function getFieldWidgets($fieldTypeName)
    {
        $widgets = [];
        foreach ($this->getAllFieldWidgets() as $widgetName => $widget) {
            if ($widget->supports($fieldTypeName)) {
                $widgets[$widgetName] = $widget;
            }
        }
        return $widgets;
    }

    /**
     * Get all field widgets.
     *
     * @return FieldWidgetInterface[]
     */
    public function getAllFieldWidgets()
    {
        if (null === $this->fieldWidgets) {
            $this->fieldWidgets = [];
            foreach ($this->getFieldWidgetProviders() as $provider) {
                foreach ($provider->getFieldWidgets() as $widget) {
                    /* @var $widget FieldWidgetBase */
                    // Make sure widget has same dispatcher/templateEngine/… as manager
                    // @TODO: This is not lazy, move to getFieldWidget() instead?
                    $widget->setDispatcher($this->getDispatcher());
                    $widget->setTemplateEngine($this->getTemplateEngine());

                    $this->fieldWidgets[$widget->getName()] = $widget;
                }
            }
        }
        return $this->fieldWidgets;
    }

    /**
     * Get field widget by name.
     *
     * @param string $name
     */
    public function getFieldWidget($name)
    {
        $fieldWidgets = $this->getAllFieldWidgets();
        return isset($fieldWidgets[$name]) ? $fieldWidgets[$name] : null;
    }

    /**
     * Register form layout provider to manager.
     *
     * @param FormLayoutProviderInterface $provider
     */
    public function registerFormLayoutProvider(FormLayoutProviderInterface $provider)
    {
        $this->formLayoutProviders[$provider->getName()] = $provider;
    }

    /**
     * Get all registered form-layout providers.
     *
     * @return FormLayoutProviderInterface[]
     */
    public function getFormLayoutProviders()
    {
        return $this->formLayoutProviders;
    }

    /**
     * Get form-layout provider by name.
     *
     * @param string $name
     */
    public function getFormLayoutProvider($name)
    {
        return isset($this->formLayoutProviders[$name]) ? $this->formLayoutProviders[$name] : null;
    }

    /**
     * Get all form layouts.
     *
     * @return FormLayoutInterface[]
     */
    public function getFormLayouts()
    {
        if (null === $this->formLayouts) {
            $this->formLayouts = [];
            foreach ($this->getFormLayoutProviders() as $provider) {
                foreach ($provider->getFormLayouts() as $name => $layout) {
                    $layout->setUuidGenerator($this->getUuidGenerator());
                    $layout->setTemplateEngine($this->getTemplateEngine());
                    $this->formLayouts[$name] = $layout;
                }
            }
        }
        return $this->formLayouts;
    }

    /**
     * Get a from layout by name.
     *
     * @param string $name
     * @return FormLayoutInterface
     */
    public function getFormLayout($name)
    {
        $layouts = $this->getFormLayouts();
        return isset($layouts[$name]) ? $layouts[$name] : null;
    }

    /**
     * @return FormLayoutOptions
     */
    public function getFormLayoutOptions()
    {
        $options = new FormLayoutOptions();
        $options->setUuidGenerator($this->getUuidGenerator());
        return $options;
    }

    /**
     * @return FormListenerProviderInterface[]
     */
    public function getFormListenerProviders()
    {
        return $this->formListenerProviders;
    }

    /**
     * Register a form listener to the manager.
     *
     * @param FormListenerProviderInterface $provider
     */
    public function registerFormListenerProvider(FormListenerProviderInterface $provider)
    {
        if ($provider instanceof DispatcherAwareInterface) {
            $provider->setDispatcher($this->getDispatcher());
        }
        $this->formListenerProviders[$provider->getName()] = $provider;
    }

    /**
     * Get form listener provider by name.
     *
     * @param string $name
     * @return FormListenerProviderInterface
     */
    public function getFormListenerProvider($name)
    {
        $formListenerProviders = $this->getFormListenerProviders();
        return isset($formListenerProviders[$name]) ? $formListenerProviders[$name] : null;
    }

    /**
     * Register an entity storage provider.
     *
     * @param EntityStorageProviderInterface $provider
     */
    public function registerEntityStorageProvider(EntityStorageProviderInterface $provider)
    {
        $this->entityStorageProviders[$provider->getName()] = $provider;
    }

    /**
     * @return EntityStorageHandlerInterface[]
     */
    public function getEntityStorageHandlers()
    {
        if (null === $this->entityStorageHandlers) {
            $this->entityStorageHandlers = [];
            foreach ($this->entityStorageProviders as $provider) {
                foreach ($provider->getEntityStorageHandlers() as $name => $handler) {
                    $this->entityStorageHandlers[$name] = $handler;
                }
            }
        }
        return $this->entityStorageHandlers;
    }

}
