<?php

namespace GO1\FormCenter\Manager;

use GO1\FormCenter\Entity\EntityManagerBase;
use GO1\FormCenter\Entity\EntityManagerInterface;

trait SubManagerAwareManagerTrait
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * Get entity manager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->entityManager = new EntityManagerBase();
        }
        return $this->entityManager;
    }

}
