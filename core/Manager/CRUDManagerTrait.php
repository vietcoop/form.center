<?php

namespace GO1\FormCenter\Manager;

/**
 * In application scope, we need hooks to application's events, this is single
 * place for it.
 *
 * We should call:
 *      $manager->create($form_submission);
 * Instead of persit it directly. This gives a chance for us to hook to form
 *  submission event, to execute the form listeners.
 *
 */
trait CRUDManagerTrait
{

    /**
     * Persit an instance.
     */
    public function create($instance)
    {

    }

    /**
     * Update an instance
     */
    public function update($instance)
    {

    }

    /**
     * Path an instance with commands.
     */
    public function patch($instance, array $commands = [])
    {

    }

    /**
     * Delete an instance.
     */
    public function delete($instance)
    {

    }

}
