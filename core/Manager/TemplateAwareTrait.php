<?php

namespace GO1\FormCenter\Manager;

use GO1\FormCenter\Helper\FilesystemLoader;
use Symfony\Component\Templating\DelegatingEngine;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;

trait TemplateAwareTrait
{

    /** @var EngineInterface */
    private $templateEngine;

    public function setTemplateEngine(EngineInterface $engine)
    {
        $this->templateEngine = $engine;
    }

    /**
     * Get template engine.
     *
     * @return EngineInterface
     */
    public function getTemplateEngine()
    {
        if (null === $this->templateEngine) {
            $engine = $this->generateDefaultTemplateEngine();
            $this->trigger('form.manager.template.engine.create', $this, ['engine' => $engine]);
            $this->setTemplateEngine($engine);
        }
        return $this->templateEngine;
    }

    protected function generateDefaultTemplateEngine()
    {
        $engine = new DelegatingEngine();
        $engine->addEngine($this->generatePHPEngine());
        return $engine;
    }

    protected function getFileSystemLoader()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../../resources/templates/%name%');
        $this->trigger('form.manager.template.engine.dirs', $this, ['loader' => $loader]);
        return $loader;
    }

    protected function generatePHPEngine()
    {
        $engine = new PhpEngine(new TemplateNameParser(), $this->getFileSystemLoader());
        return $engine;
    }

}
