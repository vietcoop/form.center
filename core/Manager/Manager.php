<?php

namespace GO1\FormCenter\Manager;

use GO1\FormCenter\Entity\EntityBase;
use GO1\FormCenter\Entity\Storage\EntityStorageHandlerInterface;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Submission\FormSubmission;
use RuntimeException;

class Manager
{

    use \GO1\FormCenter\Manager\ProviderAwareManagerTrait,
        \GO1\FormCenter\Manager\SubManagerAwareManagerTrait,
        \GO1\FormCenter\Manager\CRUDManagerTrait,
        \GO1\FormCenter\Manager\TemplateAwareTrait,
        \AndyTruong\Event\EventAwareTrait,
        \AndyTruong\Uuid\UuidGeneratorAwareTrait;

    /** @var FieldInterface */
    private $fields = [];

    /**
     * Get available fields, can be filtered by name of entity types.
     *
     * @param string[] $entityTypes
     * @return FieldInterface[]
     */
    public function getFields(array $entityTypes = [], $refresh = false)
    {
        foreach ($entityTypes as $entityTypeName) {
            if ($refresh || !isset($this->fields[$entityTypeName])) {
                $this->fields = [];
                foreach ($this->getEntityType($entityTypeName)->getFields() as $fieldName => $field) {
                    $this->fields[$entityTypeName][$fieldName] = $field;
                }
            }
        }
        return $this->fields;
    }

    /**
     * Get a field by entity name and field name.
     *
     * @param string $entityTypeName
     * @param string $fieldName
     * @return FieldInterface
     */
    public function getField($entityTypeName, $fieldName)
    {
        $fields = $this->getFields([$entityTypeName]);
        return isset($fields[$entityTypeName][$fieldName]) ? $fields[$entityTypeName][$fieldName] : null;
    }

    /**
     * Generate a form submission.
     *
     * @param FormInterface $form
     * @return FormSubmission
     */
    public function createFormSubmission(FormInterface $form = null)
    {
        $fs = new FormSubmission($this, $form);
        $fs->setDispatcher($this->getDispatcher());
        return $fs;
    }

    /**
     * Create entity object.
     *
     * @param EntityTypeInterface $entityType
     * @return EntityBase
     */
    public function createEntity(EntityTypeInterface $entityType)
    {
        $entity = new EntityBase();
        $entity->setEntityType($entityType);
        return $entity;
    }

    /**
     * @param string $entityTypeName
     * @return EntityStorageHandlerInterface
     */
    public function getEntityStorageHandler($entityTypeName)
    {
        foreach ($this->getEntityStorageHandlers() as $handler) {
            if ($handler->support($entityTypeName)) {
                return $handler;
            }
        }
        throw new RuntimeException(strtr('Can not find storage handler for !entityTypeName', ['!entityTypeName' => $entityTypeName]));
    }

}
