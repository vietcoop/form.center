<?php

namespace GO1\FormCenter\Field;

use UnexpectedValueException;

/**
 * Options for field, the object is stored with FormLayoutOptions
 */
class FieldOptions
{

    /** @var string */
    private $parent;

    /** @var string field label. If null, field's label will be used. */
    private $title;

    /** @var string Description of field in page. If null, field's description will be used. */
    private $description;

    /** @var string Help text displayed with field. If null, field's help text will be used. */
    private $help;

    /** @var int Priority of field in form page. */
    private $weight;

    /** @var string */
    private $domTagName = 'div';

    /** @var string. If null, Uuid of field in page will be used. */
    private $domId;

    /** @var string[] */
    private $domClasses = [];

    public function __construct(array $options = [])
    {
        foreach ($options as $key => $val) {
            switch ($key) {
                case 'parent':
                case 'title':
                case 'description':
                case 'help':
                case 'weight':
                case 'domTagName':
                case 'domId':
                case 'domClasses':
                    $this->{$key} = $val;
                    break;
                default:
                    throw new UnexpectedValueException('Invalid option: ' . $key);
            }
        }
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getHelp()
    {
        return $this->help;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getDomTagName()
    {
        return $this->domTagName;
    }

    public function getDomId()
    {
        return $this->domId;
    }

    public function getDomClasses()
    {
        return $this->domClasses;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setHelp($help)
    {
        $this->help = $help;
        return $this;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function setDomTagName($domTagName)
    {
        $this->domTagName = $domTagName;
        return $this;
    }

    public function setDomId($domId)
    {
        $this->domId = $domId;
        return $this;
    }

    public function setDomClasses($domClasses)
    {
        $this->domClasses = $domClasses;
        return $this;
    }

}
