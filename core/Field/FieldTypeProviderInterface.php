<?php

namespace GO1\FormCenter\Field;

use AndyTruong\Event\DispatcherAwareInterface;
use GO1\FormCenter\Field\Type\FieldTypeInterface;

interface FieldTypeProviderInterface extends DispatcherAwareInterface
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getHumanName();

    /**
     * @return FieldTypeInterface[]
     */
    public function getFieldTypes();
}
