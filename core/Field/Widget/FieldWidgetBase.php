<?php

namespace GO1\FormCenter\Field\Widget;

use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Field\FieldOptions;
use GO1\FormCenter\Field\FieldValueItemInterface;
use InvalidArgumentException;

abstract class FieldWidgetBase implements FieldWidgetInterface
{

    use \AndyTruong\Event\EventAwareTrait,
        \AndyTruong\Common\Traits\TemplateEngineAwareTrait,
        \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var string[] */
    protected $supportedFieldTypes;

    /** @var string */
    protected $templatePath;

    public function getSupportedFieldTypes()
    {
        if (null === $this->supportedFieldTypes) {
            // @TODO: need review
            $eventName = 'form.supports.' . $this->getName();
            $this->supportedFieldTypes = $this->collectResults($eventName, $this, [], [function($input) {
                    if (!is_string($input)) {
                        throw new InvalidArgumentException(sprintf('Supported field type must be a string. A %s given.', gettype($input)));
                    }
                }]);
        }
        return $this->supportedFieldTypes;
    }

    public function supports($fieldTypeName)
    {
        return in_array($fieldTypeName, $this->getSupportedFieldTypes());
    }

    public function getTemplatePath()
    {
        if (null === $this->templatePath) {
            $this->setTemplatePath('widget/base.html.php');
        }
        return $this->templatePath;
    }

    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;
    }

    /**
     * Render field.
     *
     * @param FieldInterface $field
     * @param FieldOptions $fieldOptions
     * @param FieldValueItemInterface[] $fieldValueItems Internal use only.
     * @param array $params Internal use only
     * @return string
     */
    public function render(FieldInterface $field, FieldOptions $fieldOptions, array $fieldValueItems = [], array $params = [])
    {
        $params += [
            'name'           => $field->getName(),
            'description'    => $fieldOptions->getDescription(),
            'help'           => $fieldOptions->getHelp(),
            'id'             => $fieldOptions->getDomId(),
            'required'       => $field->isRequired(),
            'tag'            => $fieldOptions->getDomTagName(),
            'title'          => $fieldOptions->getTitle(),
            'prefix'         => $field->getPrefix(),
            'suffix'         => $field->getSuffix(),
            'numberOfValues' => $field->getNumberOfValues(),
            'closeTag'       => true,
            // sub-class must define.
            'field'          => method_exists($this, 'renderFieldTypes') ? $this->renderFieldTypes($field, $fieldOptions, $fieldValueItems) : '',
        ];

        $params['classes'] = isset($params['classes']) ? $params['classes'] : [];
        $params['classes'] = array_merge($params['classes'], $fieldOptions->getDomClasses());
        $params['classes'] = implode(' ', $params['classes']);

        return $this->getTemplateEngine()->render($this->getTemplatePath(), $params);
    }

}
