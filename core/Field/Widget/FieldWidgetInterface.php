<?php

namespace GO1\FormCenter\Field\Widget;

use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Field\FieldOptions;
use GO1\FormCenter\Field\FieldValueItemInterface;

interface FieldWidgetInterface
{

    /**
     * Machine name of widget.
     *
     * @return string
     */
    public function getName();

    /**
     * Human name of widget.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Get supported field types.
     *
     * @return string[]
     */
    public function getSupportedFieldTypes();

    /**
     * Check is a field type supported.
     *
     * @param string $fieldTypeName
     * @return bool
     */
    public function supports($fieldTypeName);

    /**
     * Set template path.
     *
     * @param string $templatePath
     */
    public function setTemplatePath($templatePath);

    /**
     * Get template path
     *
     * @return string
     */
    public function getTemplatePath();

    /**
     * Render a field - used inside Field::render().
     *
     * @param FieldInterface $field
     * @param FieldOptions $fieldOptions
     * @param FieldValueItemInterface[] $fieldValueItems Internal use only.
     * @param array $params
     */
    public function render(FieldInterface $field, FieldOptions $fieldOptions, array $fieldValueItems = [], array $params = []);
}
