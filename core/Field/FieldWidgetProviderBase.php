<?php

namespace GO1\FormCenter\Field;

use GO1\FormCenter\Field\Widget\FieldWidgetInterface;
use GO1\FormCenter\FieldWidget\TextFieldWidget;
use InvalidArgumentException;

class FieldWidgetProviderBase implements FieldWidgetProviderInterface
{

    use \AndyTruong\Event\EventAwareTrait,
        \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var FieldWidgetInterface[] */
    protected $widgets;

    /**
     * @return FieldWidgetInterface[]
     */
    public function getFieldWidgets()
    {
        if (null === $this->widgets) {
            $eventName = 'fieldWidgetProvider.collect.' . $this->getName();
            $this->widgets = $this->collectResults($eventName, $this, [], [function($input) {
                if (!$input instanceof FieldWidgetInterface) {
                    throw new InvalidArgumentException('Widget must be an instance of GO1\FormCenter\Field\Widget\FieldWidgetInterface');
                }
            }]);
            $this->widgets = array_merge($this->widgets, [new TextFieldWidget()]);
        }
        return $this->widgets;
    }

}
