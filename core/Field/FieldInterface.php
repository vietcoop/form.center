<?php

namespace GO1\FormCenter\Field;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\Type\FieldTypeInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetInterface;
use Symfony\Component\Validator\ConstraintViolationList;

interface FieldInterface
{

    /**
     * Get machine name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Set field description.
     *
     * @param string $description
     */
    public function setDescription($description);

    /**
     * Get field description.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Get entity-type, which provide the field.
     *
     * @return EntityTypeInterface
     */
    public function getEntityType();

    /**
     * Add more field type.
     *
     * @param FieldTypeInterface $fieldType
     * @param string $fieldTypeUuid
     * @param int $weight
     */
    public function addFieldType(FieldTypeInterface $fieldType, $fieldTypeUuid, $weight = 0);

    /**
     * @return FieldTypeInterface[]
     */
    public function getFieldTypes();

    /**
     * In most case, field only have one field type, can ommit uuid when getting
     * field type.
     *
     * @return FieldTypeInterface
     */
    public function getFieldType($fieldTypeUuid = null);

    /**
     * @return FieldWidgetInterface
     */
    public function getFieldWidget();

    /**
     * Set prefix.
     *
     * @param string $prefix
     */
    public function setPrefix($prefix);

    /**
     * Get prefix.
     *
     * @return string
     */
    public function getPrefix();

    /**
     * Set suffix.
     * @param string $suffix
     */
    public function setSuffix($suffix);

    /**
     * Get suffix.
     *
     * @return string
     */
    public function getSuffix();

    /**
     * Set derivable.
     *
     * @param bool $derivable
     */
    public function setDerivable($derivable);

    /**
     * When build form, some fields are derivable, some (most) are not.
     *
     * For example: Static HTML field (just for markup).
     *
     * @return bool
     */
    public function isDerivable();

    /**
     * Set required property.
     *
     * @param bool $required
     */
    public function setRequired($required);

    /**
     * @return bool
     *      true if the field is required
     *      false if it's not.
     */
    public function isRequired();

    /**
     * Set number of values.
     *
     * @param int $number
     */
    public function setNumberOfValues($number);

    /**
     * Default should be 1.
     *
     * @return int
     *       0 to 'unlimited'
     *       n, n > 0, number of values.
     */
    public function getNumberOfValues();

    /**
     * Set field options, the structure of options should match
     * FieldType::getConfigSchema().
     *
     * @param FieldOptions $options
     */
    public function setFieldOptions(FieldOptions $options);

    /**
     * Get field options.
     *
     * @return array
     */
    public function getFieldOptions();

    /**
     * Set default value for field.
     *
     * @param FieldValueItemInterface $defaultValue
     */
    public function setDefaultValue(FieldValueItemInterface $defaultValue);

    /**
     * Set default value of field.
     *
     * @return FieldValueItemInterface
     */
    public function getDefaultValue();

    /**
     * Validate a field-value item.
     *
     * @param FieldValueItemInterface[] $fieldValueItems
     * @return ConstraintViolationList
     */
    public function validate(array $fieldValueItems);

    /**
     * Use widget to render field output, used inside
     * FormLayoutInterface::render().
     *
     * @param FieldOptions $fieldOptions
     * @param FieldValueItemInterface[] $fieldValueItems
     * @return string
     */
    public function render(FieldOptions $fieldOptions, array $fieldValueItems = []);
}
