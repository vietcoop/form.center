<?php

namespace GO1\FormCenter\Field;

use AndyTruong\Event\DispatcherAwareInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetInterface;

interface FieldWidgetProviderInterface extends DispatcherAwareInterface
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getHumanName();

    /**
     * @return FieldWidgetInterface[]
     */
    public function getFieldWidgets();
}
