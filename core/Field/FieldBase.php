<?php

namespace GO1\FormCenter\Field;

use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\Type\FieldTypeInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetInterface;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * @TODO A field can have multiple field-types, for example address field, which
 * include address name, country, …
 */
class FieldBase implements FieldInterface
{

    use \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var string */
    private $description;

    /** @var EntityTypeInterface */
    private $entityType;

    /** @var FieldTypeInterface[] */
    protected $fieldTypes = [];

    /** @var int[] */
    protected $fieldTypeWeights = [];

    /** @var FieldWidgetInterface */
    protected $fieldWidget;

    /** @var string */
    private $prefix;

    /** @var string */
    private $suffix;

    /** @var bool */
    private $derivable = false;

    /** @var bool */
    private $required = false;

    /** @var int */
    private $numberOfValues = 1;

    /** @var FieldValueItemInterface */
    private $defaultValue;

    /** @var array */
    private $fieldOptions;

    function getDescription()
    {
        return $this->description;
    }

    function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getEntityType()
    {
        return $this->entityType;
    }

    public function getFieldType($uuid = null)
    {
        if ((null === $uuid) && ($uuids = array_keys($this->fieldTypes))) {
            $uuid = $uuids[0];
        }
        return isset($this->fieldTypes[$uuid]) ? $this->fieldTypes[$uuid] : null;
    }

    public function getFieldWidget()
    {
        return $this->fieldWidget;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getSuffix()
    {
        return $this->suffix;
    }

    public function getNumberOfValues()
    {
        return $this->numberOfValues;
    }

    public function getFieldOptions()
    {
        return $this->fieldOptions;
    }

    public function setEntityType(EntityTypeInterface $entityType)
    {
        $this->entityType = $entityType;
        return $this;
    }

    public function addFieldType(FieldTypeInterface $fieldType, $fieldTypeUuid, $weight = 0)
    {
        $this->fieldTypes[$fieldTypeUuid] = $fieldType;
        $this->fieldTypeWeights[$fieldTypeUuid] = $weight;
        return $this;
    }

    public function getFieldTypes()
    {
        $weights = $this->fieldTypeWeights;
        uksort($this->fieldTypes, function($a, $b) use ($weights) {
            return $weights[$a] < $weights[$b] ? -1 : 1;
        });
        return $this->fieldTypes;
    }

    public function setFieldWidget(FieldWidgetInterface $fieldWidget)
    {
        $this->fieldWidget = $fieldWidget;
        return $this;
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }

    public function setDerivable($derivable)
    {
        $this->derivable = $derivable;
        return $this;
    }

    public function isDerivable()
    {
        return true === $this->derivable;
    }

    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    public function isRequired()
    {
        return true === $this->required;
    }

    public function setNumberOfValues($numberOfValues)
    {
        $this->numberOfValues = $numberOfValues;
        return $this;
    }

    public function setFieldOptions(FieldOptions $fieldOptions)
    {
        $this->fieldOptions = $fieldOptions;
        return $this;
    }

    public function setDefaultValue(FieldValueItemInterface $defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * {@inheritdoc}
     * @param FieldValueItemInterface[] $fieldValueItem
     * @return ConstraintViolationList
     */
    public function validate(array $fieldValueItems)
    {
        return $this->getFieldType()->validate($fieldValueItems);
    }

    /**
     * {@inheritdoc}
     * @param FieldOptions $fieldOptions
     * @param FieldValueItemInterface $fieldValueItems
     * @param array $fieldValueItems
     */
    public function render(FieldOptions $fieldOptions, array $fieldValueItems = [])
    {
        $this->getFieldWidget()->render($this, $fieldOptions, $fieldValueItems);
    }

}
