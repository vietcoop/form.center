<?php

namespace GO1\FormCenter\Field;

use GO1\FormCenter\Field\FieldTypeProviderInterface;
use GO1\FormCenter\FieldType\Picklist;
use GO1\FormCenter\FieldType\TextArea;
use GO1\FormCenter\FieldType\TextField;

class FieldTypeProviderBase implements FieldTypeProviderInterface
{

    use \AndyTruong\Event\EventAwareTrait,
        \AndyTruong\Common\Traits\NameAwareTrait;

    public function getFieldTypes()
    {
        return [new TextField(), new Picklist(), new TextArea()];
    }

}
