<?php

namespace GO1\FormCenter\Field;

use ArrayAccess;

interface FieldValueItemInterface extends ArrayAccess
{

    public function toArray();
}
