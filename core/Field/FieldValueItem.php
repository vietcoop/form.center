<?php

namespace GO1\FormCenter\Field;

class FieldValueItem implements FieldValueItemInterface
{

    /** @var array */
    private $itemAttributes = [];

    /**
     * Constructor.
     *
     * @param array $attrs
     */
    public function __construct(array $attrs = [])
    {
        foreach ($attrs as $offset => $value) {
            $this->offsetSet($offset, $value);
        }
    }

    public function toArray()
    {
        return $this->itemAttributes;
    }

    /**
     * {inheritdoc}
     * @param string $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->itemAttributes[] = $value;
        }
        else {
            $this->itemAttributes[$offset] = $value;
        }
    }

    /**
     * {inheritdoc}
     * @param string $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->itemAttributes[$offset]);
    }

    /**
     * {inheritdoc}
     * @param string $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->itemAttributes[$offset]);
    }

    /**
     * {inheritdoc}
     * @param string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->itemAttributes[$offset]) ? $this->itemAttributes[$offset] : null;
    }

}
