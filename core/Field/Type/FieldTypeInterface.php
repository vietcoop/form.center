<?php

namespace GO1\FormCenter\Field\Type;

use GO1\FormCenter\Field\FieldValueItemInterface;
use Symfony\Component\Validator\ConstraintViolationList;

interface FieldTypeInterface
{

    /**
     * Get machine name of field type.
     *
     * @return string
     */
    public function getName();

    /**
     * Get human name of field type.
     *
     * @return string
     */
    public function getHumanName();

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Get schema of the form. For example:
     *
     * - if field type is 'formatted text', its schema should be: [value, fomat]
     * - if field type is 'entity reference', schema: [entity_type, target_id]
     *
     * @return array
     */
    public function getSchema();

    /**
     * We need config field in form builder, each field type should provide a
     * config schema.
     *
     * For example, if the field type is integer, config schema should be:
     *
     *  [
     *      min => [label => Mininmun, description => …],
     *      max => [label => Maximum, desription => …]
     *  ]
     *
     * The schema structure should be as same as Drupal 8/Kwalify:
     *      https://www.drupal.org/node/1905070
     *
     * @return array
     */
    public function getConfigSchema();

    /**
     * @param FieldValueItemInterface $fieldValueItem
     * @return bool
     */
    public function isEmpty(FieldValueItemInterface $fieldValueItem);

    /**
     * Validate user's (single) input. If the field accepts multiple values,
     * manager should loop through and validate each single value.
     *
     * @param FieldValueItemInterface[] $fieldValueItems
     * @return ConstraintViolationList
     */
    public function validate(array $fieldValueItems);

    /**
     * Render the field.
     */
    public function render();
}
