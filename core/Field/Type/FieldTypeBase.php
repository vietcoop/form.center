<?php

namespace GO1\FormCenter\Field\Type;

abstract class FieldTypeBase implements FieldTypeInterface
{

    use \AndyTruong\Common\Traits\NameAwareTrait;

    /** @var array */
    protected $schema;

    /** @var array */
    protected $configSchema = [];

    /** @var string */
    protected $description;

    /**
     * {@inheritdoc}
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * {@inheritdoc}
     * @param array $schema
     * @return FieldTypeBase
     */
    public function setSchema($schema)
    {
        $this->schema = $schema;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * {@inheritdoc}
     * @param array $configSchema
     * @return FieldTypeBase
     */
    public function setConfigSchema($configSchema)
    {
        $this->configSchema = $configSchema;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getConfigSchema()
    {
        return $this->configSchema;
    }

    public function render()
    {
        return '';
    }

}
