<?php

namespace GO1\FormCenter\Helper;

use Symfony\Component\Templating\Loader\FilesystemLoader as BaseFilesystemLoader;

class FilesystemLoader extends BaseFilesystemLoader
{

    /**
     * @param string $templatePathPattern
     */
    public function addTemplatePathPattern($templatePathPattern)
    {
        $this->templatePathPatterns[] = $templatePathPattern;
    }

}
