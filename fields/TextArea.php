<?php

namespace GO1\FormCenter\FieldType;

class TextArea extends TextField
{

    /** @var string */
    protected $name = 'textarea';

    /** @var string */
    protected $humanName = 'Text area';

    /**
     * Constructor
     */
    public function __construct()
    {
        $configSchema = parent::getConfigSchema();
        $configSchema += [
            'rows' => ['label' => 'Rows', 'defaultValue' => 5],
        ];
    }

}
