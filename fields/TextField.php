<?php

namespace GO1\FormCenter\FieldType;

use GO1\FormCenter\Field\FieldValueItemInterface;
use GO1\FormCenter\Field\Type\FieldTypeBase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class TextField extends FieldTypeBase
{

    /** @var string */
    protected $name = 'textfield';

    /** @var string */
    protected $humanName = 'Text field';

    /** @var string[] */
    protected $schema = ['value'];

    public function __construct()
    {
        $configSchema = parent::getConfigSchema();
        $configSchema += [
            'maxLength' => ['label' => 'Max length', 'defaultValue' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     * @param FieldValueItemInterface $fieldValueItem
     * @return bool
     */
    public function isEmpty(FieldValueItemInterface $fieldValueItem)
    {
        return empty($fieldValueItem['value']);
    }

    /**
     * @param FieldValueItemInterface[] $fieldValueItems
     * @return ConstraintViolationList
     */
    public function validate(array $fieldValueItems)
    {
        $errors = new ConstraintViolationList();

        foreach ($fieldValueItems as $fieldValueItem) {
            if (!isset($fieldValueItem['value'])) {
                $msg = 'Missing value attribute.';
            }
            elseif (!is_string($fieldValueItem['value'])) {
                $msg = 'Value attribute is not a string.';
            }

            if (!empty($msg)) {
                $errors->add(new ConstraintViolation($msg, $msgTemplate = '', [], $fieldValueItem, 'value', $fieldValueItem['value']));
            }
        }

        return $errors;
    }

}
