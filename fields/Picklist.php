<?php

namespace GO1\FormCenter\FieldType;

use GO1\FormCenter\Field\FieldValueItemInterface;
use GO1\FormCenter\Field\Type\FieldTypeBase;

class Picklist extends FieldTypeBase
{

    /** @var string */
    protected $name = 'picklist';

    /** @var string */
    protected $humanName = 'Pick List';

    /** @var string[] */
    protected $schema = ['value'];

    public function __construct()
    {
        $configSchema = parent::getConfigSchema();
        $configSchema += [
            'picklistValues' => ['label'   => 'Pick list values', 'type'    => 'mapping', 'mapping' => [
                    'active'       => ['type' => 'bool'],
                    'defaultValue' => ['type' => 'bool', 'defaultValue' => false],
                    'label'        => ['type' => 'string'],
                    'value'        => ['type' => 'scalar']
                ]
            ],
        ];
    }

    public function isEmpty(FieldValueItemInterface $fieldValueItem)
    {

    }

    public function validate(array $fieldValueItems)
    {
        ;
    }

}
