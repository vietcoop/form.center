Form Builder
===============

This is library helps you to build forms with some advanced features:

- Module system: We can extends the functionality up to your needs.
- Support external entities (Salesforce, Drupal, …)
- Fields in form can be selected from multiple entity types.
- Listener per form: each form, we can add as much listeners as we need. Listener can be email/hipchat notification/webhooks/…

For visual hierarchical structure, check instruction in `./resources/uml/.gitkeep`

### Extending…

- To provide more entity types, register your provider with manager using `GO1\FormCenter\Manager\Manager::registerEntityTypeProvider()`.
- To provide more field types, register your provider with manager using `GO1\FormCenter\Manager\Manager::registerFieldTypeProvider()`.
- To provide more field widgets, register your provider with manager using `GO1\FormCenter\Manager\Manager::registerFieldWidgetProvider()`.
- …

### Event system

The library also allowed us to hooks to custom events, check `events.md` for more
informations.
