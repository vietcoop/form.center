Extends Entity Type
===============

We can not register an entity type to FOB.Manger. To do that, we have to define
a provider, which implements `GO1\FormCenter\Entity\Provider\EntityTypeProviderInterface`
or just extends `GO1\FormCenter\Entity\Provider\EntityTypeProviderBase`. In your
class, provide your logic in `getEntityTypes()`.


For example:

```
<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Entity\Provider\EntityTypeProviderBase;

class FooEntityTypeProvider extends EntityTypeProviderBase
{

    /** @var string */
    protected $name = 'foo_provider';

    /** @var string */
    protected $humanName = 'Foo Provider';

    public function discoverEntityTypes()
    {
        return [$this->getPersonEntityType()];
    }

    private function getPersonEntityType()
    {
        $entityType = new EntityTypeBase();
        $entityType->setName('testing.person');
        $entityType->setHumanName('Person');
        $entityType->setPluralHumanName('People');
        $entityType->setDescription('Person entity type.');
        $entityType->addField($this->getPersonNameField()->setEntityType($entityType));
        return $entityType;
    }

    private function getPersonNameField()
    {
        $field = new FieldBase();
        $field->setName('person_name');
        $field->setHumanName('Name of person');
        $field->setFieldType($this->dummnyFieldType());
        $field->setFieldWidget($this->dummyWidget());
        return $field;
    }

}
```
