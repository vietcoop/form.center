Event system
===============

Event name                                   | Subject                         | Params            | Description
-------------------------------------------- | ------------------------------- | ----------------- | ------------------------------------------------------
form.manager.template.engine.dirs            | Manager                         | %loader           | When new file system loader created.
form.manager.template.engine.create          | Manager                         | %event            | When new template engine created.
entityTypeProvider.stringToFieldType.`%name` | EntityTypeProviderBase          | %remoteEntityType | ~
fieldWidgetProvider.collect.`%name`          | FieldWidgetProviderInterface    | ~                 | Provider extra widget for a specific widget provider
form.supports.widget.`%widgetName`           | FieldWidgetInterface            | ~                 | `@TODO`: need review

Example on how to hook to an event:

```php
<?php
$manager = new Manager();
$manager->getDispatcher()->addListener('form.manager.template.engine.dirs', function($event) {
    /* @var $event \AndyTruong\Event\Event */
    /* @var $subject \GO1\FormCenter\Manager\Manager */
    $subject = $event->getSubject();
    $loader = $event['loader'];
});
```
