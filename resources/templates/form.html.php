<form method="<?php echo $method; ?>" action="<?php echo $action; ?>">
    <?php echo $pager; ?>

    <?php if (!empty($description)): ?>
        <div class="description">
            <?php echo $description; ?>
        </div>
    <?php endif; ?>

    <?php if (!empty($help)): ?>
        <div class="help">
            <?php echo $help; ?>
        </div>
    <?php endif; ?>

    <?php foreach ($fields as $field): ?>
        <?php echo $field; ?>
    <?php endforeach; ?>

    <input type="hidden" name="form-page" value="<?php echo $page; ?>" />
    <input type="hidden" name="form-token" value="<?php echo $token; ?>" />

    <div class="form-actions form-wrapper">
        <?php echo $buttons; ?>
    </div>
</form>
