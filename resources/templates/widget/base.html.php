<?php if ($title): ?>
    <label><?php echo $title; ?></label>
<?php endif; ?>

<?php echo "{$prefix}"; ?>

<?php // ------- Open tag ------- ?>
<?php if (!empty($closeTag)): ?>
    <?php echo '<' . $tag . ' ' . ( empty($classes) ? '' : 'class="' . $classes . '"' ) . '>'; ?>
<?php else: ?>
    <?php echo '<' . $tag . ' ' . ( empty($classes) ? '' : 'class="' . $classes . '"' ) . ' />'; ?>
<?php endif; ?>

<?php echo $field; ?>

<?php if ($help): ?>
    <div class="help"><?php echo $help; ?></div>
<?php endif; ?>

<?php if ($description): ?>
    <div class="description"><?php echo $description; ?></div>
<?php endif; ?>

<?php // ------- Close tag ------- ?>
<?php if (!empty($closeTag)): ?>
    <?php echo "</{$tag}>"; ?>
<?php endif; ?>

<?php echo "{$suffix}"; ?>
