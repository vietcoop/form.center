<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Form\Listener\FormListenerProviderBase;

class FooFormListenerProviderBase extends FormListenerProviderBase
{

    /** @var string */
    protected $name = 'foo_provider';

    /** @var string */
    protected $humanName = 'Just a foo provider';

}
