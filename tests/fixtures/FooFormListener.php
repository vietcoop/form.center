<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Form\Listener\FormListenerBase;

class FooFormListener extends FormListenerBase
{

    /** @var bool */
    protected $wildcard = true;

    /** @var array */
    public $logs = [];

    public function __construct()
    {
        $this->setName('foo_form_listener');
        $this->setHumanName('Foo form listener');
    }

    public function onWildcard()
    {
        $args = func_get_args();
        $eventName = array_shift($args);
        $this->logs[$eventName] = $args;
    }

}
