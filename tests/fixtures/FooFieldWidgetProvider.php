<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Field\FieldWidgetProviderBase;

class FooFieldWidgetProvider extends FieldWidgetProviderBase
{

    /** @var string */
    protected $name = 'foo';

    /** @var string */
    protected $humanName = 'Foo field type provider';

}
