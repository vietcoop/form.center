<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Entity\Provider\EntityTypeProviderBase;

class FooEntityTypeProvider extends EntityTypeProviderBase
{

    /** @var string */
    protected $name = 'foo_provider';

    /** @var string */
    protected $humanName = 'Foo Provider';

    public function discoverEntityTypes()
    {
        return [];
    }

}
