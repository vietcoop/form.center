<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Form\Layout\FormLayoutProviderBase;

class FooFormLayoutProvider extends FormLayoutProviderBase
{

    protected $name = 'foo_provider';
    protected $humanName = 'Foo Provider';

    public function getFormLayouts()
    {
        $layout = new FooFormLayout();
        return [$layout->getName() => $layout];
    }

}
