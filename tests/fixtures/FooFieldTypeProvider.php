<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Field\FieldTypeProviderBase;

class FooFieldTypeProvider extends FieldTypeProviderBase
{

    /** @var string */
    protected $name = 'foo';

    /** @var string */
    protected $humanName = 'Foo field type provider';

}
