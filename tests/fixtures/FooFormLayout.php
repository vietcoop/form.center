<?php

namespace GO1\FormCenter\Fixtures;

use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Form\Layout\FormLayoutHTML;

class FooFormLayout extends FormLayoutHTML
{

    public function getAction(FormInterface $form)
    {
        return '/';
    }

    /**
     * Avoid auto submissions.
     *
     * @return string
     */
    public function getToken(FormInterface $form, $pageNumber = 1)
    {
        return 'FORM-TOKEN';
    }

    public function getPager(FormInterface $form, $pageNumber)
    {
        return '<!-- Form pager -->';
    }

    public function getFormButons(FormInterface $form, $pageNumber)
    {
        return '<button>Save</button>';
    }

}
