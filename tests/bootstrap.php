<?php

use Composer\Autoload\ClassLoader;

$locations[] = __DIR__ . "/../vendor/autoload.php";
$locations[] = __DIR__ . "/../../../autoload.php";

foreach ($locations as $location) {
    if (is_file($location)) {
        /* @var $loader ClassLoader */
        $loader = require $location;
        $loader->addPsr4('GO1\\FormCenter\\TestCases\\', __DIR__ . '/form_center');
        $loader->addPsr4('GO1\\FormCenter\\Fixtures\\', __DIR__ . '/fixtures');
        break;
    }
}
