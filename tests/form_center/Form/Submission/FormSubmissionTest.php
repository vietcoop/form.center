<?php

namespace GO1\FormCenter\TestCases\Form\Submission;

use GO1\FormCenter\Field\FieldValueItem;
use GO1\FormCenter\Field\FieldValueItemInterface;
use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobFormSubmission
 * @group fobReady
 */
class FormSubmissionTest extends BaseTestCase
{

    public function testFormSubmission()
    {
        $fs = $this->dummyFormSubmission();
        $this->assertInstanceOf('GO1\FormCenter\Form\Submission\FormSubmissionInterface', $fs);
        $this->assertInstanceOf('GO1\FormCenter\Form\FormInterface', $fs->getForm());
        $this->assertSame($this->dummyManager()->getDispatcher(), $fs->getDispatcher());
    }

    /**
     * @dataProvider sourceValidate
     * @param FieldValueItemInterface $fv
     * @group x
     */
    public function testValidate(FieldValueItemInterface $fv, $errorCount)
    {
        $submission = $this->dummyFormSubmission();
        $entityType = array_keys($submission->getForm()->getEntityTypes())[0];

        $formFields = $submission->getForm()->getFields();
        $fieldUuid = array_keys($formFields)[0];
        $fieldName = $formFields[$fieldUuid]->getName();

        $submission->setFieldInput($entityType, $fieldName, $fv, 0);
        $this->assertCount($errorCount, $submission->validate());
    }

    public function sourceValidate()
    {
        return [
            [new FieldValueItem(['value' => 'Johnson English']), 0],
            [new FieldValueItem(), 1],
            [new FieldValueItem(['value' => ['Johnson', 'English']]), 1],
        ];
    }

}
