<?php

namespace GO1\FormCenter\TestCases\Form\Layout;

use GO1\FormCenter\Form\Layout\FormLayoutOptions;
use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobFormLayout
 * @group fobReady
 */
class FormLayoutTest extends BaseTestCase
{

    public function testLayout()
    {
        $layout = $this->dummyFormLayout();
        $this->assertInstanceOf('GO1\FormCenter\Form\Layout\FormLayoutInterface', $layout);
    }

    public function testRender()
    {
        $layout = $this->dummyFormLayout();
        $form = $this->dummyFormWithField();

        // Create layout-options
        $fieldUuids = array_keys($form->getFields());
        $fieldUuid = reset($fieldUuids);

        /* @var $layoutOptions FormLayoutOptions */
        $layoutOptions = $this->dummyManager()->getFormLayoutOptions();
        $pageUuid = $layoutOptions->addPage();
        $layoutOptions->addField($pageUuid, $fieldUuid);

        // Do and check
        $this->assertContains('</form>', $layout->render($form, $layoutOptions));
    }

}
