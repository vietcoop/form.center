<?php

namespace GO1\FormCenter\TestCases\Form;

use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\TestCases\BaseTestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @group fobReady
 * @group fobForm
 */
class FormTest extends BaseTestCase
{

    public function testForm()
    {
        $form = $this->dummyForm();
        $this->assertInstanceOf('GO1\FormCenter\Form\FormInterface', $form);
        $this->assertInstanceOf('GO1\FormCenter\Form\Layout\FormLayoutInterface', $form->getLayout());
    }

    public function testEntityType()
    {
        // Create the form
        $form = clone $this->dummyForm();

        // get & check
        $entityTypes = $form->getEntityTypes();
        $uuids = array_keys($entityTypes);
        $uuid = reset($uuids);
        $this->assertInstanceOf('GO1\FormCenter\Entity\Type\EntityTypeInterface', $entityTypes[$uuid]);
        $this->assertInstanceOf('GO1\FormCenter\Entity\Type\EntityTypeInterface', $form->getEntityType($uuid));

        // remove entity type & check
        $form->removeEntityType($uuid);
        $this->assertNull($form->getEntityType($uuid));
    }

    public function testListener()
    {
        // set
        $form = $this->dummyForm();
        $form->addListener($this->getMock('GO1\FormCenter\Form\Listener\FormListenerBase'));

        // get & check
        $listeners = $form->getListeners();
        $uuids = array_keys($listeners);
        $uuid = reset($uuids);
        $this->assertInstanceOf('GO1\FormCenter\Form\Listener\FormListenerInterface', $listeners[$uuid]);
        $this->assertInstanceOf('GO1\FormCenter\Form\Listener\FormListenerInterface', $form->getListener($uuid));

        // remove & check
        $form->removeListener($uuid);
        $this->assertNull($form->getListener($uuid));
    }

    public function testField($checkRemove = true)
    {
        // Create form
        $manager = $this->dummyManager();
        $form = $this->dummyFormWithField();

        $entityTypes = $form->getEntityTypes();
        $entityTypeNames = array_keys($entityTypes);
        $entityTypeName = reset($entityTypeNames);

        // Add field
        $fieldPersonName = $manager->getFields($entityTypeNames)[$entityTypeName]['person_name'];
        $uuidPersonName = $form->addField($entityTypeName, $fieldPersonName);

        // Check
        $this->assertInstanceOf('GO1\FormCenter\Field\FieldInterface', $form->getField($entityTypeName, $fieldPersonName->getName()));

        if ($checkRemove) {
            // Remove field and check again
            $form->removeField($uuidPersonName);
            $this->assertNull($form->getField($entityTypeName, $fieldPersonName->getName()));
        }

        return [$manager, $form];
    }

    public function testRender()
    {
        // Create form
        /* @var $form FormInterface */
        list(, $form) = $this->testField(false);

        // mocking
        /* @var $layout PHPUnit_Framework_MockObject_MockObject */
        $layout = $form->getLayout();
        $layout->expects($this->once())
            ->method('render')
            ->willReturn('<form isTesting="true"></form>');

        // Check
        $this->assertEquals('<form isTesting="true"></form>', $form->render());
    }

    /**
     * @dataProvider sourceOverrideFieldWidget
     */
    public function testOverrideFieldWidget($valid)
    {
        // Create form
        /* @var $form FormInterface */
        list(, $form) = $this->testField(false);
        $fields = $form->getFields();
        $fieldUuids = array_keys($fields);
        $fieldUuid = reset($fieldUuids);

        // Create new widget, change default widget for the field
        $widget = $this->getMock('GO1\FormCenter\Field\Widget\FieldWidgetBase');
        $widget->expects($this->any())->method('getHumanName')->willReturn('Mock widget');
        $widget->expects($this->once())->method('supports')->with('textfield')->willReturn($valid);

        if (false === $valid) {
            $this->setExpectedException('UnexpectedValueException', 'Mock widget is not compatible with Text field');
        }

        // change default widget
        $form->setFieldWidget($fieldUuid, $widget);
    }

    public function sourceOverrideFieldWidget()
    {
        return [[true], [false]];
    }

}
