<?php

namespace GO1\FormCenter\TestCases\Form\Listener;

use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobFormListener
 */
class FormListenerTest extends BaseTestCase
{

    public function testFormListener()
    {
        $form = $this->dummyForm();
        $listener = $this->dummyFormListener();
        $listenerUuid = $form->addListener($listener, ['foo' => 'bar']);
        $this->assertSame($listener, $form->getListener($listenerUuid));
        return [$form, $listener, $listenerUuid];
    }

    public function testOnRender()
    {
        /* @var $form \GO1\FormCenter\Form\FormInterface */
        /* @var $listener \GO1\FormCenter\Fixtures\FooFormListener */
        list($form, $listener, ) = $this->testFormListener();
        $form->render();
        $this->assertArrayHasKey('onRender', $listener->logs, 'Listener.onRender is executed.');
        $this->assertInstanceOf('GO1\FormCenter\Form\FormInterface', $listener->logs['onRender'][0]);
        $this->assertArrayHasKey('page', $listener->logs['onRender'][1]);
        $this->assertArrayHasKey('output', $listener->logs['onRender'][1]);
    }

}
