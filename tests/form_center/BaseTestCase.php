<?php

namespace GO1\FormCenter\TestCases;

use AndyTruong\Uuid\Uuid;
use GO1\FormCenter\Entity\Provider\EntityTypeProviderInterface;
use GO1\FormCenter\Entity\Type\EntityTypeBase;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use GO1\FormCenter\Field\FieldBase;
use GO1\FormCenter\Field\FieldInterface;
use GO1\FormCenter\Field\Widget\FieldWidgetBase;
use GO1\FormCenter\Fixtures\FooEntityTypeProvider;
use GO1\FormCenter\Fixtures\FooFieldTypeProvider;
use GO1\FormCenter\Fixtures\FooFieldWidgetProvider;
use GO1\FormCenter\Fixtures\FooFormLayoutProvider;
use GO1\FormCenter\Fixtures\FooFormListener;
use GO1\FormCenter\Fixtures\FooFormListenerProviderBase;
use GO1\FormCenter\Form\Form;
use GO1\FormCenter\Form\FormInterface;
use GO1\FormCenter\Manager\Manager;
use PHPUnit_Framework_TestCase;

abstract class BaseTestCase extends PHPUnit_Framework_TestCase
{

    /** @var Manager */
    private $manager;

    /** @var EntityTypeProviderInterface */
    private $entityTypeProvider;

    /** @var FormInterface  */
    private $form;

    /**
     * @return EntityTypeProviderInterface
     */
    private function dummyEntityTypeProvider()
    {
        if (null === $this->entityTypeProvider) {
            $this->entityTypeProvider = new FooEntityTypeProvider();
            $this->entityTypeProvider->registerEntityType($this->dummyEntityType());
        }
        return $this->entityTypeProvider;
    }

    /**
     * @return EntityTypeInterface
     */
    protected function dummyEntityType()
    {
        $entityType = new EntityTypeBase();
        $entityType->setName('testing.person');
        $entityType->setHumanName('Person');
        $entityType->setPluralHumanName('People');
        $entityType->setDescription('Person entity type.');
        $entityType->addField($this->dummyField()->setEntityType($entityType));
        return $entityType;
    }

    /**
     * @return FieldInterface
     */
    protected function dummyField()
    {
        $field = new FieldBase();
        $field->setName('person_name');
        $field->setHumanName('Name of person');
        $field->addFieldType($this->dummnyFieldType(), $this->dummyManager()->getUuidGenerator()->generate());
        $field->setFieldWidget($this->dummyWidget());
        return $field;
    }

    protected function dummnyFieldType()
    {
        return $this->dummyManager()->getFieldType('textfield');
    }

    /**
     * @return FieldWidgetBase
     */
    protected function dummyWidget()
    {
        return $this->dummyManager()->getFieldWidget('textfield');
    }

    protected function dummyManager()
    {
        if (null === $this->manager) {
            $this->manager = new Manager();
            $this->manager->registerFieldTypeProvider(new FooFieldTypeProvider());
            $this->manager->registerFieldWidgetProvider(new FooFieldWidgetProvider());
            $this->manager->registerFormLayoutProvider(new FooFormLayoutProvider());
            $this->manager->registerFormListenerProvider(new FooFormListenerProviderBase());
            $this->manager->setUuidGenerator(Uuid::getGenerator());
        }

        if (null === $this->entityTypeProvider) {
            if (0 === count($this->manager->getEntityTypeProviders())) {
                $this->manager->registerEntityTypeProvider($this->dummyEntityTypeProvider());
            }
        }

        return $this->manager;
    }

    protected function dummyFormLayoutOptions(FormInterface $form)
    {

        $entityTypeName = array_keys($form->getEntityTypes())[0];
        $entityType = $form->getEntityType($entityTypeName);
        $options = $this->dummyManager()->getFormLayoutOptions();
        $pageUuid = $options->addPage();
        $fieldKey = array_keys($form->getFields())[0];
        $form->getFieldByKey($fieldKey)->setEntityType($entityType);
        $options->addField($pageUuid, $fieldKey);
        return $options;
    }

    protected function dummyForm()
    {
        if (null === $this->form) {
            $this->form = new Form();
            $this->form->setUuidGenerator($this->dummyManager()->getUuidGenerator());
            $this->form->setLayout($this->getMock('GO1\FormCenter\Form\Layout\FormLayoutHTML'));
            $this->form->addEntityType($entityType = $this->dummyEntityType());
            $this->form->addField($entityType->getName(), $this->dummyField());
            $this->form->setLayoutOptions($this->dummyFormLayoutOptions($this->form));
        }
        return $this->form;
    }

    /**
     * Form with single field (person.person_name)
     *
     * @return FormInterface
     */
    protected function dummyFormWithField()
    {
        $form = clone $this->dummyForm();
        $entityTypes = $form->getEntityTypes();
        $uuids = array_keys($entityTypes);
        $entityTypeUuid = reset($uuids);

        // Add field
        $entityTypeNames = array_keys($form->getEntityTypes());
        $entityTypeName = reset($entityTypeNames);
        $fieldPersonName = $this->dummyManager()->getFields($entityTypeNames)[$entityTypeName]['person_name'];
        $form->addField($entityTypeUuid, $fieldPersonName);

        return $form;
    }

    protected function dummyFormLayout()
    {
        return $this->dummyManager()->getFormLayout('html');
    }

    protected function dummyFormSubmission()
    {
        return $this->dummyManager()->createFormSubmission($this->dummyForm());
    }

    protected function dummyFormListener()
    {
        return new FooFormListener();
    }

    public function testOk()
    {
        $this->assertTrue(true, "Ok");
    }

}
