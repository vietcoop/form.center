<?php

namespace GO1\FormCenter\TestCases\Field;

use GO1\FormCenter\Field\FieldValueItem;
use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobReady
 */
class FieldTest extends BaseTestCase
{

    public function testField()
    {
        $field = $this->dummyEntityType()->getField('person_name');
        $this->assertInstanceOf('GO1\FormCenter\Field\FieldInterface', $field);
        $this->assertInstanceOf('GO1\FormCenter\Entity\Type\EntityTypeInterface', $field->getEntityType());
        $this->assertInstanceOf('GO1\FormCenter\Field\Type\FieldTypeInterface', $field->getFieldType());
        $this->assertInstanceOf('GO1\FormCenter\Field\Widget\FieldWidgetInterface', $field->getFieldWidget());
        $this->assertEquals('person_name', $field->getName());
        $this->assertEquals('Name of person', $field->getHumanName());
        $this->assertEquals(1, $field->getNumberOfValues());
    }

    public function testFieldOptions()
    {
        $field = $this->dummyField();
        $this->assertNull($field->getFieldOptions());
        $field->setFieldOptions($this->getMock('GO1\FormCenter\Field\FieldOptions'));
        $this->assertInstanceOf('GO1\FormCenter\Field\FieldOptions', $field->getFieldOptions());
    }

    public function testDefaultValue()
    {
        $field = $this->dummyField();
        $this->assertNull($field->getDefaultValue());
        $field->setDefaultValue($this->getMock('GO1\FormCenter\Field\FieldValueItem'));
        $this->assertInstanceOf('GO1\FormCenter\Field\FieldValueItemInterface', $field->getDefaultValue());
    }

    public function testValidate()
    {
        $field = $this->dummyField();

        // Validate invalid value. Note: person_name is a textfield.
        $fieldValueItem = new FieldValueItem();
        $fieldValueItem['value'] = ['hey' => 'ya'];
        $error = $field->validate(array($fieldValueItem));
        $this->assertInstanceOf('Symfony\Component\Validator\ConstraintViolationListInterface', $error);
        $this->assertCount(1, $error);
        $this->assertEquals('Value attribute is not a string.', $error->get(0)->getMessage());

        // Now, test a valid input
        $validFieldValueItem = new FieldValueItem();
        $validFieldValueItem['value'] = 'PHP Developer';
        $noError = $field->validate(array($validFieldValueItem));
        $this->assertInstanceOf('Symfony\Component\Validator\ConstraintViolationListInterface', $noError);
        $this->assertCount(0, $noError);
    }

}
