<?php

namespace GO1\FormCenter\TestCases\Field\Widget;

use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobReady
 */
class FieldWidgetTest extends BaseTestCase
{

    public function testWidget()
    {
        $widget = $this->dummyWidget();
        $this->assertInstanceOf('GO1\FormCenter\Field\Widget\FieldWidgetInterface', $widget);
        $this->assertEquals('textfield', $widget->getName());
        $this->assertEquals('Text field', $widget->getHumanName());
        $this->assertSame($this->dummyManager()->getDispatcher(), $widget->getDispatcher());
        $this->assertTrue($widget->supports('textfield'));
        $this->assertInstanceOf('Symfony\Component\Templating\EngineInterface', $widget->getTemplateEngine());
    }

}
