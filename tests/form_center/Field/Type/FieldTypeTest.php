<?php

namespace GO1\FormCenter\TestCases\Field\Type;

use GO1\FormCenter\Field\FieldValueItem;
use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobReady
 * @group fobFieldType
 */
class FieldTypeTest extends BaseTestCase
{

    public function testFieldType()
    {
        $fieldType = $this->dummnyFieldType();
        $this->assertEquals('textfield', $fieldType->getName());
        $this->assertEquals('Text field', $fieldType->getHumanName());
    }

    public function testIsEmpty()
    {
        $emptyValue = new FieldValueItem();
        $emptyValue['value'] = '';
        $this->assertTrue($this->dummnyFieldType()->isEmpty($emptyValue));

        $nonEmptyValue = new FieldValueItem();
        $nonEmptyValue['value'] = 'John English';
        $this->assertFalse($this->dummnyFieldType()->isEmpty($nonEmptyValue));
    }

    public function testValidate()
    {
        $missingAttributeErrors = $this->dummnyFieldType()->validate([new FieldValueItem()]);
        $this->assertCount(1, $missingAttributeErrors);
        $this->assertEquals('Missing value attribute.', $missingAttributeErrors->get(0)->getMessage());

        $invalidStringValue = new FieldValueItem();
        $invalidStringValue['value'] = ['foo' => 'bar'];
        $invalidStringErrors = $this->dummyField()->validate([$invalidStringValue]);
        $this->assertCount(1, $invalidStringErrors);
        $this->assertEquals('Value attribute is not a string.', $invalidStringErrors->get(0)->getMessage());
    }

}
