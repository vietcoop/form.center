<?php

namespace GO1\FormCenter\TestCases\Manager;

use GO1\FormCenter\TestCases\BaseTestCase;

/**
 * @group fobReady
 * @group fobManager
 */
class ManagerTest extends BaseTestCase
{

    public function testManager()
    {
        $manager = $this->dummyManager();
        $this->assertInstanceOf('GO1\FormCenter\Manager\Manager', $manager);
        $this->assertInstanceOf('Symfony\Component\EventDispatcher\EventDispatcherInterface', $manager->getDispatcher());
        $this->assertInstanceOf('GO1\FormCenter\Entity\Provider\EntityTypeProviderInterface', $manager->getEntityTypeProvider('foo_provider'));
        $this->assertInstanceOf('GO1\FormCenter\Form\Layout\FormLayoutProviderInterface', $manager->getFormLayoutProvider('foo_provider'));
        $this->assertInstanceOf('GO1\FormCenter\Entity\Type\EntityTypeInterface', $manager->getEntityType('testing.person'));
        $this->assertInstanceOf('GO1\FormCenter\Field\Type\FieldTypeInterface', $manager->getFieldType('textfield'));
        $this->assertInstanceOf('GO1\FormCenter\Field\Type\FieldTypeInterface', $manager->getFieldType('picklist'));
        $this->assertInstanceOf('GO1\FormCenter\Field\Type\FieldTypeInterface', $manager->getFieldType('textarea'));
        $this->assertInstanceOf('GO1\FormCenter\Entity\EntityManagerInterface', $manager->getEntityManager());
        $this->assertInstanceOf('GO1\FormCenter\Field\FieldInterface', $manager->getFields(['testing.person'])['testing.person']['person_name']);
        $this->assertInstanceOf('GO1\FormCenter\Form\Listener\FormListenerProviderInterface', $manager->getFormListenerProvider('foo_provider'));
        $this->assertInstanceOf('Symfony\Component\Templating\EngineInterface', $manager->getTemplateEngine());
        $this->assertTrue($manager->getTemplateEngine()->supports('form.html.php'));
    }

    /**
     * A single dispatcher should be used through all services.
     */
    public function testDispatcher()
    {
        $manager = $this->dummyManager();
        $m_dispatcher = $manager->getDispatcher();
        $p_dispatcher = $manager->getEntityTypeProvider('foo_provider')->getDispatcher();
        $this->assertSame($p_dispatcher, $m_dispatcher);
    }

}
