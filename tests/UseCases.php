<?php

use GO1\FormCenter\Entity\Provider\EntityTypeProviderBase;
use GO1\FormCenter\Form\Form;
use GO1\FormCenter\Form\Layout\FormLayoutHTML;
use GO1\FormCenter\Form\Submission\FormSubmission;
use GO1\FormCenter\Manager\Manager;

// SampleEntityProvider provide two entity types:
//  - User(Id, Name, Mail)
//  - Article(ID, <User>Author, Title, Body)

$manager = new Manager();

// ---------------------
// (1) Entity provider
// ---------------------
$manager->registerEntityTypeProvider('sample', $provider = new EntityTypeProviderBase());

// ---------------------
// (2) Admin create new form to let visitor contributes article
// ---------------------
$form = new Form();

$entity_types = [];
foreach ($manager->getEntityTypeProvider('sample')->getEntityTypes() as $entity_type) {
    $entity_types[] = $entity_type->getName();
}

// Add node->title field to form
$field_title = $manager->getFields($entity_types)['node->title'];
$uuid_title = $form->addField($field_title);

// Add node->body field to form
$field_body = $manager->getFields($entity_types)['node->body'];
$uuid_body = $form->addField($field_body);

// ---------------------
// (3) Config a field
// ---------------------
$form->getField($uuid_title)
    ->setFieldOptions(['maxLength' => 128])
    ->setRequired(true);
$form->getField($uuid_body)->setNumberOfValues(1);

// ---------------------
// (4) Render the form
// ---------------------
$form->setLayout($layout = new FormLayoutHTML());
$output = $form->getLayout()->render($form);

// ---------------------
// (5) User submit the form
// (6) Form listener
// ---------------------
$input = [
    $uuid_title => 'First article',
    $uuid_body  => ['value' => 'Body of the article, it should be longer…', 'format' => 'markdown'],
];

$submission = new FormSubmission();
$submission->setForm($form);
foreach ($input as $uuid_field => $field_value) {
    $submission->setFieldInput($field_uuid, $field_value, $delta = 0);
}

// Listen to submission.created
$manager->getDispatcher()->addListener('GO1\FormCenter\Form\Submission\FormSubmissionInterface.create', function() {
    // do things
});

$errors = $submission->validate();
if (0 == $errors->count()) {
    // use manager to persit the submission to fire listeners.
    $manager->create($submission);
}

// ---------------------
// (7) Details of validation:
//
//  \GO1\FormCenter\Form\Submission\FormSubmissionInterface::validate()
//      -> validate all entities in form-submisison.
//
//  Loop FieldInterface in \GO1\FormCenter\Entity\EntityInterface::getEntityType()->getFields()
//      -> \GO1\FormCenter\Field\FieldInterface::validate($fieldValueItem)
//
// ---------------------
